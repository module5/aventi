<?php

namespace Aventi\SAP\Model;

/**
 * Class OrderTypeManagment
 *
 * @package Aventi\SAP\Model
 */
class OrderTypeManagment implements \Aventi\SAP\Api\OrderTypeManagmentInterface
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $session;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     *
     * @param \Magento\Checkout\Model\Session $session
     */
    public function __construct(
        \Magento\Checkout\Model\Session $session,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->session = $session;
        $this->logger = $logger;
    }
    
    /**
     * {@inheritdoc}
     */
    public function saveOrderOrderType(
        $param
    ) {        
        $quote = $this->session->getQuote();
        $billAddress  =  $quote->getBillingAddress();
        $billAddress->setData('order_type',$param);
        $billAddress->save();

        $shippingAddress  =  $quote->getShippingAddress();
        $shippingAddress->setData('order_type',$param);
        $shippingAddress->save();

        return "ok";
    }
}