<?php declare(strict_types=1);
namespace Aventi\Imagen\Cron;

use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;

class Update
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    /**
     * @var \Aventi\Imagen\Model\Process
     */
    private $process;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Aventi\Imagen\Model\Process $process
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Aventi\Imagen\Model\Process $process
)
    {
        $this->logger = $logger;
        $this->process = $process;
    }

    public function execute()
    {
        $this->logger->info("Image cron job was executed.");
        try {
            $this->process->update();
        } catch (FileSystemException | LocalizedException $e) {
            $this->logger->debug('There was an error while syncing images: ' . $e->getMessage());
        }
    }
}

