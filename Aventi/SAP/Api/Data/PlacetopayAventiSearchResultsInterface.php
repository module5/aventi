<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aventi\SAP\Api\Data;

interface PlacetopayAventiSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get PlacetopayAventi list.
     * @return \Aventi\SAP\Api\Data\PlacetopayAventiInterface[]
     */
    public function getItems();

    /**
     * Set increment_id list.
     * @param \Aventi\SAP\Api\Data\PlacetopayAventiInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

