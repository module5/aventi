<?php
namespace Aventi\SAP\Cron;

use Bcn\Component\Json\Exception\ReadingError;
use Magento\Framework\Exception\FileSystemException;

class Stock
{
    protected $logger;

    /**
     * @var \Aventi\SAP\Model\Sync\Stock
     */
    private $_stock;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Aventi\SAP\Model\Sync\Stock $stock
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Aventi\SAP\Model\Sync\Stock $stock
    )
    {
        $this->logger = $logger;
        $this->_stock = $stock;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $this->logger->info("Stock cron job was executed.");
        try {
            $this->_stock->syncStock();
        } catch (ReadingError | FileSystemException $e) {
            $this->logger->debug('There was an error while syncing stock: ' . $e->getMessage());
        }
    }
}

