<?php

namespace Aventi\SAP\Cron;

use Bcn\Component\Json\Exception\ReadingError;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\NoSuchEntityException;

class Product
{
    protected $logger;
    /**
     * @var \Aventi\SAP\Model\Sync\Product
     */
    private $product;

    /**
     * Constructor
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Aventi\SAP\Model\Sync\Product $product
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Aventi\SAP\Model\Sync\Product $product
    ) {
        $this->logger = $logger;
        $this->product = $product;
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function execute()
    {
        $this->logger->info("Product cron job was executed.");
        try {
            $date = "1900-01-01";
            $this->product->syncProduct($date);
        } catch (ReadingError | FileSystemException | NoSuchEntityException $e) {
            $this->logger->debug('There was an error while syncing products: ' . $e->getMessage());
        }
    }
}

