<?php

namespace Aventi\SAP\Console\Command;

use Bcn\Component\Json\Exception\ReadingError;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Price extends Command
{
    const PROCESS_SELECTED = 1;

    /**
     * @var \Aventi\SAP\Model\Sync\Price
     */
    private $price;

    /**
     * @var DateTime
     */
    private $dateTime;

    public function __construct(
        \Aventi\SAP\Model\Sync\Price $price,
        DateTime $dateTime
    ) {
        parent::__construct();
        $this->price = $price;
        $this->dateTime = $dateTime;
    }

    /**
     * {@inheritdoc}
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws InputException
     * @throws ReadingError
     * @throws FileSystemException
     * @throws LocalizedException
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $customDate = "1900-01-01";
        $process = (int) $input->getArgument(self::PROCESS_SELECTED);
        if ($process < 0 || $process > 1) {
            $output->writeln("<error>Process no found :(</error>");
            throw new InputException(__('Invalid argument.'));
        } elseif ($process == 0) {
            $customDate = date('Y-m-d', strtotime($this->dateTime->date('Y-m-d')));
        }
        $this->price->setOutput($output);
        $this->price->syncPrice($customDate);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("aventi:sap:price");
        $this->setDescription("Sync SAP price to Magento. Argument: 0 => Price, 1 => Price Fast");
        $this->setDefinition([
            new InputArgument(self::PROCESS_SELECTED, null, "Process"),
        ]);
        parent::configure();
    }
}
