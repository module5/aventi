<?php

namespace Aventi\SAP\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Data
 *
 * @package Aventi\SAP\Helper
 */
class Data extends AbstractHelper
{
    /**
     * Definition of consts
     */
    const XML_PATH_PTK_SAP_PASSWORD_CUSTOMER = 'sap/setting/customer_password';
    const XML_PATH_PTK_SAP_PATH = 'sap/setting/url';
    const XML_PATH_PTK_SAP_USERNAME = 'sap/setting/username';
    const XML_PATH_PTK_SAP_PASSWORD = 'sap/setting/password';

    const PATH_TOKEN = 'token';
    const PATH_API_CUSTOMER = '/api/customer';
    const PATH_API_PRODUCT = '/api/Producto';
    const PATH_API_CARTERA = '/api/Cliente/Cartera';
    const PATH_API_STOCK = '/api/Producto/Stock';

    const PATH_EMAIL = 'sap/customer/sendemail';
    const PATH_CC = 'sap/customer/copy';

    const PATH_SERIE = 'sap/document/serie';
    const PATH_WHSCODE = 'sap/document/whscode';
    const PATH_CARDCODE = 'sap/document/cardcode';
    const PATH_SHIPPINGCODE = 'sap/document/shipping';
    const PATH_SLPCODE = 'sap/document/slpcode';
    const PATH_COSTCENTER = 'sap/document/costcenter';
    const PATH_SALESTYPE = 'sap/document/salestype';
    const PATH_TEST = 'sap/setting/test';

    const PATH_OCRCODE = 'sap/document/ocrcode';
    const PATH_GROUPCODE = 'sap/document/groupcode';
    const PATH_QRYGROUP2 = 'sap/document/qrygroup2';
    const PATH_QRYGROUP3 = 'sap/document/qrygroup3';
    const PATH_USEREST = 'sap/document/u_ser_est';
    const PATH_USERPE = 'sap/document/u_ser_pe';
    const PATH_UNUMAUTOR = 'sap/document/u_num_autor';
    const PATH_UTIPOCOMPROB = 'sap/document/u_tipo_comprob';
    const PATH_UDOCDECLARABLE = 'sap/document/u_doc_declarable';
    const PATH_PAYMENTSERIE = 'sap/document/paymentserie';

    const PATH_CASHONDELIVERY = 'sap/document/cashondelivery';
    const PATH_BANKTRANSFER = 'sap/document/banktransfer';
    const PATH_CREDIT = 'sap/document/credit';

    const PATH_ONDELIVERYTYPE = 'sap/document/ondeliverytype';
    const PATH_TRANSFERTYPE = 'sap/document/transfertype';
    const PATH_CREDITTYPE = 'sap/document/credittype';

    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    private $curl;
    /**
     * @var
     */
    private $token=null;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    /**
     * @var
     */
    private $destinationDirectory;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    private $directoryList;
    /**
     * @var \Magento\Framework\Filesystem
     */
    private $filesystem;

    /**
     * Data constructor.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\HTTP\Client\Curl $curl
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Filesystem $filesystem
    ) {
        parent::__construct($context);
        $this->curl = $curl;
        $this->filesystem = $filesystem;
        $this->logger = $logger;
        $this->directoryList = $directoryList;
        $this->destinationDirectory = $this->filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR);
    }

    public function sendEmail($store = null)
    {
        return (int)$this->scopeConfig->getValue(self::PATH_EMAIL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    public function copyEmail($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_CC, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * Return the default serie to create document in SAP
     * @return string
     */
    public function getSerie($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_SERIE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }
    /**
     * Return the default warehouse code to items to create document in SAP
     * @return string
     */
    public function getWhsCode($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_WHSCODE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * Return the default CardCode create document in SAP
     * @return string
     */
    public function getCardCode($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_CARDCODE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * Return the Shipping Product Code to create document in SAP
     * @return string
     */
    public function getShippingCode($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_SHIPPINGCODE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * Return the CostCenter to create document in SAP
     * @return string
     */
    public function getCostCenter($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_COSTCENTER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * Return the SlpCode to create document in SAP
     * @return string
     */
    public function getSlpCode($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_SLPCODE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * Return the SalesType to create document in SAP
     * @return string
     */
    public function getSalesType($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_SALESTYPE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token): void
    {
        $this->token = $token;
    }

    /**
     * @param null $store
     * @author Carlos Hernan Aguilar Hurtado <caguilar@aventi.co>
     * @return string
     */
    public function getPassword($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::XML_PATH_PTK_SAP_PASSWORD, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @author Carlos Hernan Aguilar Hurtado <caguilar@aventi.co>
     * @return string
     */
    public function getUsername($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::XML_PATH_PTK_SAP_USERNAME, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @author Carlos Hernan Aguilar Hurtado <caguilar@aventi.co>
     * @return string
     */
    public function getPath($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::XML_PATH_PTK_SAP_PATH, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return string
     */
    public function getOcrCode($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_OCRCODE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return string
     */
    public function getGroupCode($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_GROUPCODE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return string
     */
    public function getQryGroup2($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_QRYGROUP2, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return string
     */
    public function getQryGroup3($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_QRYGROUP3, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return string
     */
    public function getUserEst($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_USEREST, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return string
     */
    public function getUserPe($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_USERPE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return string
     */
    public function getNumAutor($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_UNUMAUTOR, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return string
     */
    public function getTipoComprob($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_UTIPOCOMPROB, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return string
     */
    public function getDocDeclarable($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_UDOCDECLARABLE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return string
     */
    public function getBankTransfer($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_BANKTRANSFER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return string
     */
    public function getCashOnDelivery($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_CASHONDELIVERY, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return string
     */
    public function getCredit($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_CREDIT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return string
     */
    public function getCreditType($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_CREDITTYPE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return string
     */
    public function getTransferType($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_TRANSFERTYPE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return string
     */
    public function getOnDeliveryType($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_ONDELIVERYTYPE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return string
     */
    public function getPaymentSerie($store = null)
    {
        return (int)$this->scopeConfig->getValue(self::PATH_PAYMENTSERIE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @author Carlos Hernan Aguilar Hurtado <caguilar@aventi.co>
     * @return string
     */
    public function getPasswordForCustomer($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::XML_PATH_PTK_SAP_PASSWORD_CUSTOMER, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return bool
     */
    public function getIsTest($store = null)
    {
        return (boolean)$this->scopeConfig->getValue(self::PATH_TEST, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * Generate token
     * @author Carlos Hernan Aguilar Hurtado <caguilar@aventi.co>
     *
     * @return bool
     */
    public function generateToken()
    {
        $url = $this->getPath();
        if (!empty($url)) {
            $url = $url . '/' . self::PATH_TOKEN;
            $this->curl->post($url, [
                'Username' => $this->getUsername(),
                'Password' => $this->getPassword(),
                'grant_type' => 'password'
            ]);
            $response = $this->curl->getBody();

            if ($this->curl->getStatus()  == 400) {
            } elseif ($this->curl->getStatus() == 200) {
                $responseArray = json_decode($response, true);
                $this->setToken($responseArray['access_token']);
                return true;
            } else {
                $this->setToken(null);
                return false;
            }
        } else {
            $this->logger->error('Modulo Aventi::SAP url indefinida');
        }
        return false;
    }

    /**
     * @param string $path
     * @param bool $generateToken
     * @param int $tries
     * @return bool|string
     */
    public function getRecourse($path = '', $tries=0)
    {
        try {
            if (!empty($this->getPath())) {
                $headers = [
                    "Content-Type" => "application/json"
                ];

                if (!$this->getIsTest()) {
                    if ($this->getToken() == null) {
                        $this->generateToken();
                    }
                    $headers = [
                        "Content-Type" => "application/json",
                        "Content-Length" => "200",
                        "Authorization" =>  "Bearer {$this->getToken()}"
                    ];
                }

                $this->curl->setHeaders($headers);
                $url = $this->getPath() . '/' . $path;
                $this->curl->get($url);
                if ($this->curl->getStatus() == 200) {
                    return $this->curl->getBody();
                } elseif ($this->curl->getStatus() == 401) {
                    $this->generateToken();
                    if ($tries == 0) {
                        return $this->getRecourse($path, 1);
                    }
                } else {
                    $this->_logger->error($this->curl->getBody() . ' ' . $this->curl->getStatus());
                    return false;
                }
            }
        } catch (\Exception $e) {
            $this->_logger->error($e->getCode() . $e->getMessage());
        }
    }

    public function postRecourse($path = '', $params)
    {
        try {
            if (!empty($this->getPath())) {
                $headers = [

                ];
                if (!$this->getIsTest()) {
                    if ($this->getToken() == null) {
                        $this->generateToken();
                    }
                    $headers = [
                        "Authorization" => "Bearer {$this->getToken()}"
                    ];
                }

                $this->urlRequest = $this->getPath() . '/' . $path;

                $this->curl->setHeaders($headers);
                $this->curl->post($this->getPath() . '/' . $path, $params);
                return [
                    'status' => $this->curl->getStatus(),
                    'body' => $this->curl->getBody()
                ];
            } else {
                return [
                    'status' => 500,
                    'body' => _('Server not configured')
                ];
            }
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage());
        }
    }

    /**
     * @param $data
     * @return false|string
     */
    public function getRecourseSelf($data)
    {
        try {
            $fileName = sprintf('%s%s.json', str_replace('/', '_', $data), date("YmdHis"));
            $jsonPath = $this->directoryList->getPath('var') . sprintf('/Aventi/%s', $fileName);
            $json = $this->getRecourse($data);
            if ($json != false) {
                try {
                    $this->destinationDirectory->writeFile("Aventi/{$fileName}", $json);
                } catch (\Exception $e) {
                    $this->logger->error('writeFile::' . $e->getMessage());
                }
                if (file_exists($jsonPath) && filesize($jsonPath) > 10) {
                    return $jsonPath;
                } else {
                    return false;
                }
            }
        } catch (\Exception $e) {
            $this->logger->error('getRecourse::' . $e->getMessage());
        }
    }
}
