<?php
namespace Aventi\SAP\Cron;

use Bcn\Component\Json\Exception\ReadingError;
use Magento\Framework\Exception\FileSystemException;

class PriceFast
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Aventi\SAP\Model\Sync\Price
     */
    private $_price;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $date;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Aventi\SAP\Model\Sync\Price $price
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Aventi\SAP\Model\Sync\Price $price,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
    ) {
        $this->logger = $logger;
        $this->_price = $price;
        $this->date = $dateTime;
    }

    /**
     * @return void
     */
    public function execute()
    {
        $this->logger->info("Fast price cron job was executed.");
        try {
            $currentDate = date('Y-m-d', strtotime($this->date->date('Y-m-d')));
            $this->_price->syncPrice($currentDate);
        } catch (ReadingError | FileSystemException $e) {
            $this->logger->debug('There was an error while syncing prices - Fast: ' . $e->getMessage());
        }
    }
}

