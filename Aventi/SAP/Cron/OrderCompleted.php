<?php
namespace Aventi\SAP\Cron;

class OrderCompleted
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Aventi\SAP\Model\Sync\SendToSAP
     */
    private $_sendToSAP;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Aventi\SAP\Model\Sync\SendToSAP $sendToSAP
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Aventi\SAP\Model\Sync\SendToSAP $sendToSAP
    ) {
        $this->logger = $logger;
        $this->_sendToSAP = $sendToSAP;
    }

    /**
     * @return void
     */
    public function execute()
    {
        $this->logger->info("Order cron job was executed.");
        $this->_sendToSAP->completedOrderToSAP();
    }
}
