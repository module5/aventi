<?php

namespace Aventi\SAP\Model\Sync;

use Aventi\SAP\Helper\Attribute;
use Aventi\SAP\Helper\Data;
use Aventi\SAP\Model\AbstractSync;
use Bcn\Component\Json\Exception\ReadingError;
use Bcn\Component\Json\Reader;
use Exception;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem\DriverInterface;
use Psr\Log\LoggerInterface;

class Price extends AbstractSync
{
    const WEBSERVICE = 2;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Data
     */
    private $data;

    /**
     * @var Check
     */
    private $check;

    /**
     * @param Attribute $attributeDate
     * @param LoggerInterface $logger
     * @param DriverInterface $driver
     * @param ProductRepositoryInterface $productRepository
     * @param Data $data
     * @param Check $check
     */
    public function __construct(
        Attribute $attributeDate,
        LoggerInterface $logger,
        DriverInterface $driver,
        ProductRepositoryInterface $productRepository,
        Data $data,
        Check $check,
        \Magento\Catalog\Model\ResourceModel\Product $productResource
    ) {
        parent::__construct($attributeDate, $logger, $driver, $productResource);
        $this->productRepository = $productRepository;
        $this->data = $data;
        $this->check = $check;
    }

    /**
     * Update the price product
     *
     * @param string $date
     * @throws ReadingError
     * @throws FileSystemException
     */
    public function syncPrice(string $date)
    {
        $resTable = ['check' => 0, 'updated' => 0, 'new' => 0];
        $start = 0;
        $rows = 1000;
        $flag = true;
        $method = 'api/Producto/ListasPrecios/%s/%s/%s/%s';
        $webService = Price::WEBSERVICE;
        while ($flag) {
            $jsonPath =  $this->data->getRecourseSelf(sprintf($method, $start, $rows, $date, $webService));
            if ($jsonPath) {
                $reader = $this->getJsonReader($jsonPath);
                $reader->enter(Reader::TYPE_OBJECT);
                $total = $reader->read("total");
                $products = $reader->read("data");
                $progressBar = $this->startProgressBar($total);
                foreach ($products as $product) {
                    $dataProduct = (object) [
                        'sku' => $product['ItemCode'],
                        'price' => (int) $product['Price']
                    ];
                    $response =  $this->managerPrice($dataProduct);
                    $resTable['check'] += $response['check'];
                    $resTable['updated'] += $response['updated'];
                    $this->advanceProgressBar($progressBar);
                    //Only for debug.
                    //$total--;
                }
                $start += $rows;
                $this->finishProgressBar($progressBar, $start, $rows);
                $progressBar = null;
                $this->closeFile($jsonPath);
                if ($total <= 0) {
                    $flag = false;
                }
            } else {
                $flag = false;
            }
        }
        $this->printTable($resTable);
    }

    /**
     * Check or update the price of product.
     *
     * @param object $data The product data (sku and price).
     * @return array Returns a result depending on if the product was checked
     * or updated.
     */
    public function managerPrice(object $data): array
    {
        $result = ['updated' => 0, 'empty' => 0, 'check' => 0];
        try {
            $item = $this->productRepository->get($data->sku, false, 0, false);
            $resultCheck = $this->check->checkData($data, $item, 'price', 1);
            if (!$resultCheck) {
                $result['check'] = 1;
                return $result;
            }
            $result['updated'] = 1;
            $this->_saveFields($item, $resultCheck);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
        return $result;
    }
}
