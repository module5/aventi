define([
    'jquery',
    'Magento_Customer/js/model/authentication-popup',
    'Magento_Customer/js/customer-data'
], function ($, authenticationPopup, customerData) {
    'use strict';

    return function (config, element) {
        $(element).click(function (event) {
            var cart = customerData.get('cart'),
                customer = customerData.get('customer');

            event.preventDefault();

            if (!customer().firstname && cart().isGuestCheckoutAllowed === false) {
                authenticationPopup.showModal();

                return false;
            }else {
                if (!$("#code").val() == null || !$("#code").val() == '') {
                    var customurl = BASE_URL+'proof/index/index';
                    $.ajax({
                        url: customurl,
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            code: $('#code').val()
                        },
                        complete: function(response) {        
                            console.log(response);   
                        },
                        error: function (xhr, status, errorThrown) {
                            console.log(xhr+' --- '+status+' ------ '+errorThrown);
                            return false;
                        }
                    });
                }
                $(element).attr('disabled', true);
                location.href = config.checkoutUrl;
            }
        });

    };
});
