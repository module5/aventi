<?php

namespace Aventi\SAP\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

/**
 * Class Data
 *
 * @package Aventi\SAP\Helper
 */
class DataEmail extends AbstractHelper
{
    const PATH_STORE = 'general/store_information/name';
    const PATH_URL = 'web/secure/base_url';
    const PATH_EMAIL = 'trans_email/ident_general/email';

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    private $transportBuilder;
    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    private $inlineTranslation;
    /**
     * @var Data
     */
    private $data;

    /**
     * DataEmail constructor.
     * @param Context $context
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param Data $data
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Aventi\SAP\Helper\Data $data
    ) {
        parent::__construct($context);
        $this->logger = $logger;
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->data = $data;
    }

    /**
     * @param null $store
     * @return string
     */
    public function getNameStore($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_STORE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return string
     */
    public function getUrlStore($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_URL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    /**
     * @param null $store
     * @return string
     */
    public function getEmail($store = null)
    {
        return (string)$this->scopeConfig->getValue(self::PATH_EMAIL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }
    /**
     * @param $email
     * @param $name
     * @param $password
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     */
    public function sendEmail($email, $name, $password)
    {
        $sender = [
            'name' => $this->getNameStore(),
            'email' =>  $this->getEmail()
        ];
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $transport = $this->transportBuilder
            ->setTemplateIdentifier('sap_register') // this code we have mentioned in the email_templates.xml
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND, // this is using frontend area to get the template file
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                ]
            )
            ->setTemplateVars(
                [
                    'email' => $email,
                    'name' => $name,
                    'password' => $password
                ]
            )
            ->setFrom($sender);
        if ($this->data->copyEmail() != '') {
            if (filter_var($this->data->copyEmail(), FILTER_VALIDATE_EMAIL)) {
                $transport->addCc($this->data->copyEmail());
                if ($this->data->sendEmail() == 0) {
                    $email = $this->data->copyEmail();
                }
            }
        }
        $transport = $transport->addTo($email)
                               ->getTransport();
        if ($this->data->sendEmail() == 1) {
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } elseif ($this->data->copyEmail() != '') {
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        }
    }
    /**
     * Send the email  order in coming
     *
     * @param $email
     * @param $name
     * @param $order
     * @throws \Magento\Framework\Exception\MailException
     */
    public function sendOrderEmail($email, $name, $order, $tracker, $address, $city, $html)
    {
        $sender = [
            'name' => $this->getNameStore(),
            'email' =>  $this->getEmail()
        ];
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $transport = $this->transportBuilder
            ->setTemplateIdentifier('order_sent') // this code we have mentioned in the email_templates.xml
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND, // this is using frontend area to get the template file
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                ]
            )
            ->setTemplateVars(
                [
                    'email' => $email,
                    'name' => $name,
                    'order' => $order,
                    'tracker' => $tracker,
                    'address' => $address,
                    'city' => $city,
                    'html' => $html
                ]
            )
            ->setFrom($sender)
            ->addTo($email)
            ->getTransport();
        $transport->sendMessage();
        $this->inlineTranslation->resume();
    }
    /**
     * Send the email cancel order
     *
     * @param $email
     * @param $name
     * @param $order
     * @throws \Magento\Framework\Exception\MailException
     */
    public function sendOrderCancelEmail($email, $name, $orderId, $payment, $order)
    {
        $sender = [
            'name' => $this->getNameStore(),
            'email' => $this->getEmail()
        ];
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $transport = $this->transportBuilder
            ->setTemplateIdentifier('order_cancel') // this code we have mentioned in the email_templates.xml
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND, // this is using frontend area to get the template file
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                ]
            )
            ->setTemplateVars(
                [
                    'email' => $email,
                    'name' => $name,
                    'orderId' => $orderId,
                    'payment' => $payment,
                    'order' => $order
                ]
            )
            ->setFrom($sender)
            ->addTo($email)
            ->getTransport();
        $transport->sendMessage();
        $this->inlineTranslation->resume();
    }

    public function sendEmailBase($data=[], $templateId, $email)
    {
        $sender = [
            'name' => $this->getNameStore(),
            'email' =>  $this->getEmail()
        ];
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $transport = $this->transportBuilder
            ->setTemplateIdentifier($templateId) // this code we have mentioned in the email_templates.xml
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND, // this is using frontend area to get the template file
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                ]
            )
            ->setTemplateVars(
                $data
            )
            ->setFrom($sender)
            ->addTo($email)
            ->getTransport();
        $transport->sendMessage();
        $this->inlineTranslation->resume();
    }
}
