<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aventi\SAP\Model\Data;

use Aventi\SAP\Api\Data\PlacetopayAventiInterface;

class PlacetopayAventi extends \Magento\Framework\Api\AbstractExtensibleObject implements PlacetopayAventiInterface
{

    /**
     * Get placetopayaventi_id
     * @return string|null
     */
    public function getPlacetopayaventiId()
    {
        return $this->_get(self::PLACETOPAYAVENTI_ID);
    }

    /**
     * Set placetopayaventi_id
     * @param string $placetopayaventiId
     * @return \Aventi\SAP\Api\Data\PlacetopayAventiInterface
     */
    public function setPlacetopayaventiId($placetopayaventiId)
    {
        return $this->setData(self::PLACETOPAYAVENTI_ID, $placetopayaventiId);
    }

    /**
     * Get increment_id
     * @return string|null
     */
    public function getIncrementId()
    {
        return $this->_get(self::INCREMENT_ID);
    }

    /**
     * Set increment_id
     * @param string $incrementId
     * @return \Aventi\SAP\Api\Data\PlacetopayAventiInterface
     */
    public function setIncrementId($incrementId)
    {
        return $this->setData(self::INCREMENT_ID, $incrementId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Aventi\SAP\Api\Data\PlacetopayAventiExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Aventi\SAP\Api\Data\PlacetopayAventiExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Aventi\SAP\Api\Data\PlacetopayAventiExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get value
     * @return string|null
     */
    public function getValue()
    {
        return $this->_get(self::VALUE);
    }

    /**
     * Set value
     * @param string $value
     * @return \Aventi\SAP\Api\Data\PlacetopayAventiInterface
     */
    public function setValue($value)
    {
        return $this->setData(self::VALUE, $value);
    }

    /**
     * Get status
     * @return string|null
     */
    public function getStatus()
    {
        return $this->_get(self::STATUS);
    }

    /**
     * Set status
     * @param string $status
     * @return \Aventi\SAP\Api\Data\PlacetopayAventiInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }
}

