<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aventi\SAP\Plugin\PlacetoPay\Payments\Model;

use Aventi\SAP\Api\Data\PlacetopayAventiInterfaceFactory;
use Aventi\SAP\Model\PlacetopayAventiRepository;
use Dnetix\Redirection\Message\RedirectInformation;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order;
use Psr\Log\LoggerInterface;

class PaymentMethod
{

    /**
     * @var LoggerInterface
     */
    private $_logger;
    /**
     * @var PlacetopayAventiRepository
     */
    private $_placetopayAventiRepository;
    /**
    * @var PlacetopayAventiInterfaceFactory
    */
    private $_placetopayAventiInterfaceFactory;

    public function __construct(
        LoggerInterface $logger,
        PlacetopayAventiInterfaceFactory $placetopayAventiInterfaceFactory,
        PlacetopayAventiRepository $placetopayAventiRepository
    ) {
        $this->_logger = $logger;
        $this->_placetopayAventiRepository = $placetopayAventiRepository;
        $this->_placetopayAventiInterfaceFactory = $placetopayAventiInterfaceFactory;
    }

    /**
     * @param \PlacetoPay\Payments\Model\PaymentMethod $subject
     * @param RedirectInformation $information
     * @param Order               $order
     * @param Order\Payment       $payment
     * @return array
     */
    public function beforeSettleOrderStatus(
        \PlacetoPay\Payments\Model\PaymentMethod $subject,
        RedirectInformation $information,
        &$order,
        $payment = null
    ) {
        try {
            $placetoPayAventi = $this->_placetopayAventiInterfaceFactory->create();
            $placetoPayAventi->setIncrementId($order->getIncrementId());
            $placetoPayAventi->setStatus($information->status()->status());
            $placetoPayAventi->setValue(json_encode($information->toArray()));
            $this->_placetopayAventiRepository->save($placetoPayAventi);
        } catch (LocalizedException $e) {
            $this->_logger->log("error", "Error al guardar datos de Placetopay: " . $e->getMessage());
        }
        return [
            $information, $order, $payment
        ];
    }
}
