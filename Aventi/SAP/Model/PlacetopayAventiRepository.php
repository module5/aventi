<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aventi\SAP\Model;

use Aventi\SAP\Api\Data\PlacetopayAventiInterfaceFactory;
use Aventi\SAP\Api\Data\PlacetopayAventiSearchResultsInterfaceFactory;
use Aventi\SAP\Api\PlacetopayAventiRepositoryInterface;
use Aventi\SAP\Model\ResourceModel\PlacetopayAventi as ResourcePlacetopayAventi;
use Aventi\SAP\Model\ResourceModel\PlacetopayAventi\CollectionFactory as PlacetopayAventiCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class PlacetopayAventiRepository implements PlacetopayAventiRepositoryInterface
{

    private $storeManager;

    protected $dataObjectProcessor;

    protected $placetopayAventiCollectionFactory;

    protected $dataPlacetopayAventiFactory;

    private $collectionProcessor;

    protected $placetopayAventiFactory;

    protected $extensibleDataObjectConverter;
    protected $searchResultsFactory;

    protected $extensionAttributesJoinProcessor;

    protected $resource;

    protected $dataObjectHelper;


    /**
     * @param ResourcePlacetopayAventi $resource
     * @param PlacetopayAventiFactory $placetopayAventiFactory
     * @param PlacetopayAventiInterfaceFactory $dataPlacetopayAventiFactory
     * @param PlacetopayAventiCollectionFactory $placetopayAventiCollectionFactory
     * @param PlacetopayAventiSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourcePlacetopayAventi $resource,
        PlacetopayAventiFactory $placetopayAventiFactory,
        PlacetopayAventiInterfaceFactory $dataPlacetopayAventiFactory,
        PlacetopayAventiCollectionFactory $placetopayAventiCollectionFactory,
        PlacetopayAventiSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->placetopayAventiFactory = $placetopayAventiFactory;
        $this->placetopayAventiCollectionFactory = $placetopayAventiCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPlacetopayAventiFactory = $dataPlacetopayAventiFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Aventi\SAP\Api\Data\PlacetopayAventiInterface $placetopayAventi
    ) {
        /* if (empty($placetopayAventi->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $placetopayAventi->setStoreId($storeId);
        } */
        
        $placetopayAventiData = $this->extensibleDataObjectConverter->toNestedArray(
            $placetopayAventi,
            [],
            \Aventi\SAP\Api\Data\PlacetopayAventiInterface::class
        );
        
        $placetopayAventiModel = $this->placetopayAventiFactory->create()->setData($placetopayAventiData);
        
        try {
            $this->resource->save($placetopayAventiModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the placetopayAventi: %1',
                $exception->getMessage()
            ));
        }
        return $placetopayAventiModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($placetopayAventiId)
    {
        $placetopayAventi = $this->placetopayAventiFactory->create();
        $this->resource->load($placetopayAventi, $placetopayAventiId);
        if (!$placetopayAventi->getId()) {
            throw new NoSuchEntityException(__('PlacetopayAventi with id "%1" does not exist.', $placetopayAventiId));
        }
        return $placetopayAventi->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->placetopayAventiCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Aventi\SAP\Api\Data\PlacetopayAventiInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Aventi\SAP\Api\Data\PlacetopayAventiInterface $placetopayAventi
    ) {
        try {
            $placetopayAventiModel = $this->placetopayAventiFactory->create();
            $this->resource->load($placetopayAventiModel, $placetopayAventi->getPlacetopayaventiId());
            $this->resource->delete($placetopayAventiModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the PlacetopayAventi: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($placetopayAventiId)
    {
        return $this->delete($this->get($placetopayAventiId));
    }
}

