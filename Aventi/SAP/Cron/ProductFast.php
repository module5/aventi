<?php
namespace Aventi\SAP\Cron;

use Bcn\Component\Json\Exception\ReadingError;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\NoSuchEntityException;

class ProductFast
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Aventi\SAP\Model\Sync\Product
     */
    private $_product;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $date;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Aventi\SAP\Model\Sync\Product $product
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Aventi\SAP\Model\Sync\Product $product,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
    ) {
        $this->logger = $logger;
        $this->_product = $product;
        $this->date = $dateTime;
    }

    /**
     * Execute the cron
     *
     * @return void
     * @throws \Exception
     */
    public function execute()
    {
        $this->logger->info("Fast product cron job was executed.");
        try {
            $currentDate = date('Y-m-d', strtotime($this->date->date('Y-m-d')));
            $this->_product->syncProduct($currentDate);
        } catch (ReadingError | FileSystemException | NoSuchEntityException $e) {
            $this->logger->debug('There was an error while syncing products - Fast: ' . $e->getMessage());
        }
    }
}

