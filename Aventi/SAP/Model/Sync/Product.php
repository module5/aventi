<?php

namespace Aventi\SAP\Model\Sync;

use Aventi\SAP\Helper\Attribute;
use Aventi\SAP\Helper\Data;
use Aventi\SAP\Helper\SAP;
use Aventi\SAP\Model\AbstractSync;
use Bcn\Component\Json\Exception\ReadingError;
use Bcn\Component\Json\Reader;
use Exception;
use Magento\Catalog\Api\CategoryLinkManagementInterface;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\ProductFactory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\State;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Filesystem\DriverInterface;
use Magento\Tax\Model\TaxClass\Source\Product as ProductTax;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\Area as AppArea;

class Product extends AbstractSync
{
    const WEBSERVICE = 2;

    /**
     * @var Data
     */
    private $data;

    /**
     * @var ProductTax
     */
    private $productTaxClassSource;

    /**
     * @var ProductRepositoryInterface
     */
    private $_productRepository;

    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * @var SendToSAP
     */
    private $helperSAP;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var CategoryLinkManagementInterface
     */
    private $categoryLinkManagement;

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var Check
     */
    private $check;

    /**
     * @var Configurable
     */
    private $configurable;

    /**
     * @var State
     */
    private $state;

    /**
     * Product constructor.
     * @param DriverInterface $driver
     * @param Data $data
     * @param LoggerInterface $logger
     * @param ProductTax $productTaxClassSource
     * @param ProductRepositoryInterface $productRepository
     * @param Attribute $attribute
     * @param ProductFactory $productFactory
     * @param SendToSAP $helperSAP
     * @param CategoryLinkManagementInterface $categoryLinkManagementInterface
     * @param ResourceConnection $resourceConnection
     * @param CategoryRepository $categoryRepository
     * @param Check $check
     * @param Configurable $configurable
     * @param \Magento\Catalog\Model\ResourceModel\Product $productResource
     * @param State $state
     */
    public function __construct(
        DriverInterface $driver,
        Data $data,
        LoggerInterface $logger,
        ProductTax $productTaxClassSource,
        ProductRepositoryInterface $productRepository,
        Attribute $attribute,
        ProductFactory $productFactory,
        SAP $helperSAP,
        CategoryLinkManagementInterface $categoryLinkManagementInterface,
        ResourceConnection $resourceConnection,
        CategoryRepository $categoryRepository,
        Check $check,
        Configurable $configurable,
        \Magento\Catalog\Model\ResourceModel\Product $productResource,
        State $state
    ) {
        parent::__construct($attribute, $logger, $driver, $productResource);
        $this->data = $data;
        $this->logger = $logger;
        $this->productTaxClassSource = $productTaxClassSource;
        $this->_productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->helperSAP = $helperSAP;
        $this->categoryLinkManagement = $categoryLinkManagementInterface;
        $this->resourceConnection = $resourceConnection;
        $this->categoryRepository = $categoryRepository;
        $this->check = $check;
        $this->configurable = $configurable;
        $this->state = $state;
    }

    /**
     * Management for data product (configurable, categories).
     * Creating or updating process.
     * @param object $data The data previously loaded from SAP.
     * @return bool
     */
    public function productManager(object $data): bool
    {
        try {
            $item = $this->_productRepository->get($data->sku, false, 0, false);
            $hasParent = $this->validateParent($item->getId());
            $visibility = ($hasParent) ? 1 : 4;
            $resultCheck = $this->check->checkData($data, $item, 'product', $visibility);
            $checkCategories = $this->check->checkCategories($data, $item);
            if (!$resultCheck && !$checkCategories) {
                return true;
            }
            if ($resultCheck) {
                $this->_saveFields($item, $resultCheck);
            }
            if ($checkCategories) {
                $this->categoryLinkManagement->assignProductToCategories($data->sku, $data->categoryId);
            }
        } catch (NoSuchEntityException $e) {
            $this->createProduct($data);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
        return false;
    }

    /**
     * Get the tax_id
     *
     * @param $value
     * @return null
     */
    public function getTaxId($value): ?int
    {
        $taxClasses = $this->productTaxClassSource->getAllOptions();

        foreach ($taxClasses as $tax) {
            if ($tax['label'] == $value) {
                return $tax['value'];
            }
        }
        return 0;
    }

    /**
     * Generate pretty url by products
     *
     * @param $name
     * @return string
     */
    public function generateURL($name): string
    {
        $url = preg_replace('#[^0-9a-z]+#i', '-', $name);
        $url = strtolower($url);
        $connection = $this->resourceConnection->getConnection();
        $connection->getTableName('url_rewrite');
        $sql = <<<SQL
            SELECT count(*) from url_rewrite where request_path = '_URL_.html'
SQL;
        $sql = str_replace('_URL_', $url, $sql);
        $results = $this->resourceConnection->getConnection()->fetchOne($sql);

        if ($results > 0) {
            return $url . '-' . $this->generateRandomString(5);
        }
        return $url;
    }

    /**
     * Generate in string for the url product
     *
     * @param int $length
     * @return bool|string
     */
    public function generateRandomString(int $length = 10)
    {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, $length);
    }

    /**
     * @param string $date
     * @throws FileSystemException
     * @throws NoSuchEntityException
     * @throws ReadingError
     * @throws Exception
     */
    public function syncProduct(string $date)
    {
        $start = 0;
        $rows = 1000;
        $flag = true;
        $webService = Stock::WEBSERVICE;
        $method = 'api/Producto/%s/%s/%s/%s';
        while ($flag) {
            $jsonPath = $this->data->getRecourseSelf(sprintf($method, $start, $rows, $date, $webService));
            if ($jsonPath) {
                $reader = $this->getJsonReader($jsonPath);
                $reader->enter(null, Reader::TYPE_OBJECT);
                $total = $reader->read("total");
                $products = $reader->read("data");
                $progressBar = $this->startProgressBar($total);
                foreach ($products as $product) {
                    $state = $product['frozenFor'] ?? '';
                    $status = \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED;
                    if ($state == 'N') {
                        $status = \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED;
                    }
                    $categoryChild = $this->helperSAP->getLastCategory($product['U_CG3_Magento_Ult_Categoria']);
                    $categoryArray = [];
                    if ($categoryChild) {
                        $categoryParent = $this->getParentCategory($categoryChild);
                        $categoryArray = $this->resolveCategories($categoryParent, $categoryChild);
                    }
                    $codeColor = $product['Codecolor'] ?? '';
                    $color = isset($product['Color']) ? strtoupper($product['Color']) : '';
                    $size = $product['U_ARGNS_SIZE'] ?? '';
                    $style = isset($product['ESTILO']) ? ucwords(strtolower($product['ESTILO'])) : '';
                    $fit = isset($product['Cuerpo']) ? ucwords(strtolower($product['Cuerpo'])) : '';
                    $composition = $product['U_ARGNS_Comp'] ?? '';
                    $nameMagento = $product['U_CG3_Magento_Nombre'] ?? '';
                    $description = $product['U_CG3_Magento_Descripcion'] ?? '';

                    $product = (object) [
                        'sku' =>  isset($product['ItemCode']) ? str_replace(' ', '', $product['ItemCode']) : '',
                        'name' => $product['ItemName'] ?? '',
                        'parentName' => isset($product['FrgnName']) ? $product['FrgnName'] . ' ' . strtoupper($color) : '',
                        'tax' => isset($product['TaxCodeAR']) ? $this->getTaxId($product['TaxCodeAR']) : 0,
                        'status' =>  $status,
                        'price' => 0,
                        'categoryId' => $categoryArray,
                        'codeBars' => $product['CodeBars'] ?? '',
                        'weight' => $product['SWeight1'] ?? 0,
                        'parentModel' => isset($product['U_ARGNS_MOD']) ? $product['U_ARGNS_MOD'] . '-' . $codeColor : 0,
                        'nameMagento' => $nameMagento,
                        'color' => ($color) ? $this->getOptionId($color, 'color') : 0,
                        'talla' => ($size) ? $this->getOptionId($size, 'talla') : 0,
                        'estilo' => ($style) ? $this->getOptionId($style, 'estilo') : 0,
                        'fit' => ($fit) ? $this->getOptionId($fit, 'fit') : 0,
                        'shortDescription' => $composition,
                        'description' => $description
                    ];

                    $this->state->emulateAreaCode(
                        AppArea::AREA_ADMINHTML,
                        function () use ($product) {
                            $this->productManager($product);
                        }
                    );
                    $this->advanceProgressBar($progressBar);
                    //Only for debug.
                    //$total--;
                }
                $start += $rows;
                $this->finishProgressBar($progressBar, $start, $rows);
                $progressBar = null;
                $this->closeFile($jsonPath);
                if ($total <= 0) {
                    $flag = false;
                }
            } else {
                $flag = false;
            }
        }
    }

    /**
     * @param $childCategory
     * @return CategoryInterface|null
     * @throws NoSuchEntityException
     */
    public function getParentCategory($childCategory): ?CategoryInterface
    {
        return $this->categoryRepository->get($childCategory);
    }

    /**
     * @param $parent
     * @param $child
     * @return array
     */
    public function resolveCategories($parent, $child): array
    {
        return [$parent, $child];
    }

    /**
     * @param $productData
     * @throws CouldNotSaveException
     * @throws InputException
     * @throws StateException
     */
    public function assocToConfigurableProduct($productData)
    {
        $configurableProduct = $this->createOrGetConfigurableProduct($productData);
        $parentModel = $productData->parentModel;
        try {
            if ($configurableProduct) {
                $assoc = [];
                $simpleProduct = $this->_productRepository->get($productData->sku);
                $configurableProduct = $this->_productRepository->get($parentModel);
                $children = $configurableProduct->getTypeInstance()->getChildrenIds($configurableProduct->getId());
                foreach ($children as $child) {
                    foreach ($child as $c) {
                        $assoc[] = $c;
                    }
                }
                $assoc[] = (int)$simpleProduct->getId();
                $configurableProduct->setAssociatedProductIds($assoc);
                $configurableProduct->setCanSaveConfigurableAttributes(true);
                $this->_productRepository->save($configurableProduct);
            }
        } catch (NoSuchEntityException $e) {
            $this->logger->error("Error asignando producto simple a configurable: " . $e->getMessage());
        }
    }

    public function createOrGetConfigurableProduct($productData)
    {
        try {
            if ($configurableProduct = $this->_productRepository->get($productData->parentModel)) {
                return $configurableProduct;
            }
        } catch (NoSuchEntityException $e) {
            $configurableProduct = $this->productFactory->create();
            $configurableProduct->setSku($productData->parentModel);
            $configurableProduct->setName($productData->parentName);
            $configurableProduct->setTypeId('configurable');
            $configurableProduct->setData('price', 0);
            $configurableProduct->setStoreId(0);
            $configurableProduct->setData('website_ids', [1]);
            $configurableProduct->setVisibility(4);
            $configurableProduct->setAttributeSetId(9);
            $configurableProduct->setCustomAttribute('tax_class_id', $productData->tax);
            $configurableProduct->setStatus($productData->status);
            $configurableProduct->setQuantityAndStockStatus(['qty' => 0, 'is_in_stock' => 1]);
            $configurableProduct->setUrlKey($this->generateURL($productData->name));
            $configurableProduct->setDescription($productData->description);
            if ($productData->style) {
                $configurableProduct->setData('style', $productData->estilo);
            }
            $tallaAttrId = $configurableProduct->getResource()->getAttribute('size')->getId();
            $colorAttrId = $configurableProduct->getResource()->getAttribute('color')->getId();
            $configurableProduct->getTypeInstance()->setUsedProductAttributeIds(
                [$tallaAttrId, $colorAttrId],
                $configurableProduct
            );
            $configurableAttributesData = $configurableProduct->getTypeInstance()
                ->getConfigurableAttributesAsArray($configurableProduct);
            $configurableProduct->setCanSaveConfigurableAttributes(true);
            $configurableProduct->setConfigurableAttributesData($configurableAttributesData);

            try {
                $this->_productRepository->save($configurableProduct);
            } catch (Exception $e) {
                $this->logger->error("El product {$productData->parentModel} no creo " . $e->getMessage());
            }

            try {
                if ($productData->categoryId) {
                    $this->categoryLinkManagement->assignProductToCategories(
                        $productData->parentModel,
                        $productData->categoryId
                    );
                }
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
            }
            return $configurableProduct;
        }
    }

    /**
     * Creates the product and assigns it to the respective categories.
     * @param object $data The data previously loaded from SAP.
     */
    private function createProduct(object $data)
    {
        $item = $this->productFactory->create();
        $item->setSku($data->sku);
        $item->setName($data->name);
        $item->setStoreId(0);
        $item->setPrice($data->price);
        $item->setTypeId(Type::TYPE_SIMPLE);
        $item->setAttributeSetId(9);
        $item->setStatus($data->status);
        $item->setVisibility(1);
        $item->setDescription($data->description);
        $item->setCustomAttributes([
            'tax_class_id' => $data->tax,
            'color' => $data->color,
            'size' => $data->talla,
            'style' => $data->estilo,
            'fit' => $data->fit,
            'bar_code' => $data->codeBars
        ]);
        $item->setUrlKey($this->generateURL($data->name));
        try {
            $this->_productRepository->save($item);
            $this->categoryLinkManagement->assignProductToCategories($data->sku, $data->categoryId);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * @param $productId
     * @return bool
     */
    private function validateParent($productId)
    {
        $parentIds = $this->configurable->getParentIdsByChild($productId);
        $parentId = array_shift($parentIds);
        return !is_null($parentId);
    }
}
