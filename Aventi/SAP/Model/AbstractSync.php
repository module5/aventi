<?php

namespace Aventi\SAP\Model;

use Aventi\SAP\Helper\Attribute;
use Bcn\Component\Json\Reader;
use Exception;
use Magento\Catalog\Model\ResourceModel\Product;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem\DriverInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractSync
{
    private $arrayOption = [];

    /**
     * @var Attribute
     */
    private $attributeDate;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var OutputInterface
     */
    private $output = null;

    /**
     * @var DriverInterface
     */
    protected $driver;

    /**
     * @var false|resource
     */
    private $file;

    /**
     * @var Product
     */
    private $productResource;

    /**
     * @param Attribute $attributeDate
     * @param LoggerInterface $logger
     * @param DriverInterface $driver
     * @param Product $productResource
     */
    public function __construct(
        Attribute $attributeDate,
        LoggerInterface $logger,
        DriverInterface $driver,
        Product $productResource
    ) {
        $this->attributeDate = $attributeDate;
        $this->logger = $logger;
        $this->driver = $driver;
        $this->productResource = $productResource;
    }

    /**
     * @return OutputInterface|null
     */
    private function getOutput(): ?OutputInterface
    {
        return $this->output;
    }

    /**
     * @param OutputInterface $output
     */
    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;
    }

    /**
     * Opens a resource file and returns its instance.
     * @throws FileSystemException
     * @return Reader|boolean
     */
    protected function getJsonReader($filePath): Reader
    {
        try {
            if ($this->driver->isExists($filePath)) {
                $this->file = $this->driver->fileOpen($filePath, 'r');
                return new Reader($this->file);
            }
        } catch (FileSystemException $e) {
            throw new FileSystemException(__('An error has occurred: ' . $e->getMessage()));
        }
        return false;
    }

    /**
     * Close a resource file and deletes it.
     * @param $file
     * @throws FileSystemException
     */
    protected function closeFile($file)
    {
        try {
            $this->driver->fileClose($this->file);
            $this->driver->deleteFile($file);
        } catch (FileSystemException $e) {
            throw new FileSystemException(__('An error has occurred: ' . $e->getMessage()));
        }
    }

    /**
     * Creates a ProgressBar instance and returns it.
     * @param $total
     * @return null|ProgressBar
     */
    protected function startProgressBar($total): ?ProgressBar
    {
        $out = $this->getOutput();
        if ($out) {
            $progressBar = new ProgressBar($out, $total);
            $progressBar->start();
            return $progressBar;
        }
        return null;
    }

    /**
     * @param $progressBar
     */
    protected function advanceProgressBar($progressBar)
    {
        if ($this->getOutput()) {
            $progressBar->advance();
        }
    }

    /**
     * Finish the progressBar.
     * @param $progressBar
     * @param $start
     * @param $rows
     */
    protected function finishProgressBar($progressBar, $start, $rows)
    {
        $out = $this->getOutput();
        if ($out) {
            $progressBar->finish();
            $out->writeln(sprintf("\nInteraction %s", ($start / $rows)));
        }
    }

    /**
     * Get or create the option by attributes and returns id.
     *
     * @param string $label
     * @param string $attributeCode
     * @return false|int|mixed
     */
    public function getOptionId(string $label = '', string $attributeCode = '')
    {
        try {
            if (!empty($label)) {
                $brand = str_replace(' ', '', $label);
                $optionId = 0;
                if (!array_key_exists($brand, $this->arrayOption)) {
                    $optionId = $this->attributeDate->getOptionId($attributeCode, $label);
                    if (!$optionId) {
                        $optionId = $this->attributeDate->createOrGetId($attributeCode, $label);
                    }
                    $this->arrayOption[$brand] = $optionId;
                } else {
                    $optionId = $this->arrayOption[$brand];
                }
                return $optionId;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
        return false;
    }

    /**
     * Print Table with synchro results.
     * @param $response
     */
    public function printTable($response)
    {
        $out = $this->getOutput();
        if ($out) {
            $out->writeln("\n");
            $table = new Table($out);
            $table->setRows([
                ['Data New', $response['new']],
                ['Data Updated', $response['updated']],
                ['Data Check', $response['check']]
            ]);
            $table->render();
        }
    }

    /**
     * Saves the fields with the values to update. The fields
     * can be the Product or Price.
     * @param array $checkData The data previously checked with the fields.
     */
    public function _saveFields($item, array $checkData)
    {
        foreach ($checkData as $key => $field) {
            $item->setData($key, $field);
            try {
                $item->getResource()->saveAttribute($item, $key);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }
    }
}
