<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aventi\SAP\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface PlacetopayAventiRepositoryInterface
{

    /**
     * Save PlacetopayAventi
     * @param \Aventi\SAP\Api\Data\PlacetopayAventiInterface $placetopayAventi
     * @return \Aventi\SAP\Api\Data\PlacetopayAventiInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Aventi\SAP\Api\Data\PlacetopayAventiInterface $placetopayAventi
    );

    /**
     * Retrieve PlacetopayAventi
     * @param string $placetopayaventiId
     * @return \Aventi\SAP\Api\Data\PlacetopayAventiInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($placetopayaventiId);

    /**
     * Retrieve PlacetopayAventi matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Aventi\SAP\Api\Data\PlacetopayAventiSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete PlacetopayAventi
     * @param \Aventi\SAP\Api\Data\PlacetopayAventiInterface $placetopayAventi
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Aventi\SAP\Api\Data\PlacetopayAventiInterface $placetopayAventi
    );

    /**
     * Delete PlacetopayAventi by ID
     * @param string $placetopayaventiId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($placetopayaventiId);
}

