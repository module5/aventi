<?php

namespace Aventi\SAP\Console\Command;

use Exception;
use Magento\Framework\Exception\InputException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncOrder extends Command
{
    const PROCESS_SELECTED = "Process";

    /**
     * @var \Aventi\SAP\Model\Sync\SendToSAP
     */
    private $sendToSAP;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * SyncOrder constructor.
     * @param \Aventi\SAP\Model\Sync\SendToSAP $sendToSAP
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Aventi\SAP\Model\Sync\SendToSAP $sendToSAP,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->sendToSAP = $sendToSAP;
        $this->logger = $logger;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("aventi:sync:order");
        $this->setDescription("Synchronize orders to SAP");
        $this->setHelp('This command allows you to create an or' .
            'der in SAP [1: Sync completed orders to SAP, 2: Sync failed orders to SAP ]');
        $this->setDefinition([
            new InputArgument(self::PROCESS_SELECTED, null, "Process"),
        ]);
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     * @throws InputException
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $process = (int)$input->getArgument(self::PROCESS_SELECTED);
        if ($process < 0 || $process > 2) {
            $output->writeln("<error>Process no found :(</error>");
            throw new InputException(__('Invalid argument.'));
        }
        $this->sendToSAP->setOutput($output);
        try {
            switch ($process) {
                case 1:
                    $this->sendToSAP->completedOrderToSAP();
                    break;
                case 2:
                    $this->sendToSAP->errorOrderToSAP();
                    break;
            }
        } catch (Exception $e) {
            $output->writeln("<error>{$e->getMessage()}(</error>");
        }
        $output->writeln("<info> finished :) number " . $process . '</info>');
    }
}
