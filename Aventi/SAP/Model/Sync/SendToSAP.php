<?php

namespace Aventi\SAP\Model\Sync;

use Bcn\Component\Json\Reader;
use Magento\Sales\Api\Data\OrderInterface;
use Symfony\Component\Console\Helper\ProgressBar;

class SendToSAP
{
    const CASHONDELIVERYPAYMENT = 'cashondelivery';
    const PLACETOPAYPAYMENT = 'placetopay';
    const BANKTRANSFERPAYMENT = 'banktransfer';
    const CREDIT_CARD = "CreditCard";
    const CREDIT_CARD_NUMBER = "CreditCardNumber";
    const CARD_VALID_UNTIIL = "CardValidUntil";
    const OWNER_ID_NUM = "OwnerIdNum";
    const USER_FIELDS = "CamposUsuario";

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Status\History\CollectionFactory
     */
    private $historyCollectionFactory;

    /**
     * @var \Aventi\SAP\Logger\Logger
     */
    private $logger;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    private $orderCollectionFactory;

    /**
     * @var \Aventi\SAP\Helper\Data
     */
    private $data;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    private $dataToSAP;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Magento\Directory\Model\RegionFactory
     */
    private $regionFactory;

    /**
     * @var \Magento\SalesRule\Model\Coupon
     */
    private $coupon;

    /**
     * @var \Magento\SalesRule\Model\RuleRepository
     */
    private $ruleRepository;

    /**
     * @var \Aventi\SAP\Helper\SAP
     */
    private $sap;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $dateTime;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var \Symfony\Component\Console\Output\OutputInterface
     */
    private $output = false;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var \Aventi\SAP\Model\ResourceModel\PlacetopayAventi\CollectionFactory
     */
    private $_placetopayAventiCollectionFactory;

    private $_placetopayArrayFormat;

    /**
     * SendToSAP constructor.
     * @param \Magento\Sales\Model\ResourceModel\Order\Status\History\CollectionFactory $historyCollectionFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Aventi\SAP\Helper\Data $data
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Magento\SalesRule\Model\Coupon $coupon
     * @param \Magento\SalesRule\Model\RuleRepository $ruleRepository
     * @param \Aventi\SAP\Helper\SAP $sap
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Sales\Api\OrderManagementInterface $orderManagement
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Aventi\SAP\Model\ResourceModel\PlacetopayAventi\CollectionFactory $placetopayAventiCollectionFactory
     */
    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\Status\History\CollectionFactory $historyCollectionFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Aventi\SAP\Helper\Data $data,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\SalesRule\Model\Coupon $coupon,
        \Magento\SalesRule\Model\RuleRepository $ruleRepository,
        \Aventi\SAP\Helper\SAP $sap,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Aventi\SAP\Model\ResourceModel\PlacetopayAventi\CollectionFactory $placetopayAventiCollectionFactory
    ) {
        $this->historyCollectionFactory = $historyCollectionFactory;
        $this->logger = $logger;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->data = $data;
        $this->orderRepository = $orderRepository;
        $this->customerRepository = $customerRepository;
        $this->regionFactory = $regionFactory;
        $this->coupon = $coupon;
        $this->ruleRepository = $ruleRepository;
        $this->sap = $sap;
        $this->dateTime = $dateTime;
        $this->resourceConnection = $resourceConnection;
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
        $this->_placetopayAventiCollectionFactory = $placetopayAventiCollectionFactory;
        $this->emptyPlacetoPayFormat();
    }

    /**
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    public function setOutput($output)
    {
        $this->output = $output;
    }

    /**
     * @return \Symfony\Component\Console\Output\OutputInterface
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * Out in console
     */
    public function write($paramns)
    {
        $output = $this->getOutput();
        if ($output) {
            if (is_array($paramns)) {
                $output->writeln(print_r($paramns, true));
            } else {
                $output->writeln($paramns);
            }
        }
    }

    /**
     * Send the order to SAP
     *
     * @return array
     * @author  Carlos Hernan Aguilar <caguilar@aventi.co>
     * @date 29/11/18
     */
    public function completedOrderToSAP()
    {
        return $this->processOrder(['pending' , 'syncing', 'processing']);
    }

    /**
     * @return array
     * @author Carlos Hernan Aguilar <caguilar@aventi.co>
     * @date 22/08/2019
     */
    public function errorOrderToSAP()
    {
        return $this->processOrder(['error_creacion']);
    }

    /**
     * Delete the registers of interations with SAP and return the number of interations
     *
     * @param string $status
     * @param int $orderId
     * @return int
     * @author  Carlos Hernan Aguilar <caguilar@aventi.co>
     * @date 26/11/18
     */
    public function getNumberInteration($status = 'pending', $orderId = 123456)
    {
        $iterations = 0;
        $historiesModel = $this->historyCollectionFactory->create();
        $historiesModel->addFieldToFilter('entity_name', 'order');
        $historiesModel->addFieldToFilter('status', $status);
        $historiesModel->addFieldToFilter('parent_id', $orderId);
        $historiesModel->addFieldToFilter('comment', ['like' => '%Sincronizando%']);
        $historiesModel->load();

        foreach ($historiesModel as $history) {
            $iterations = intval(preg_replace('/[^0-9]+/i', '', $history->getData('comment')));
            if ($iterations != 10) {
                $history->delete();
            }
        }
        $historiesModel->save();
        return ++$iterations;
    }

    /**
     * Validate if the error description
     *
     * @param string $description
     * @param int $orderId
     * @return int|void
     */
    public function validateError($description = 'pending', $orderId = 123456)
    {
        $historiesModel = $this->historyCollectionFactory->create();
        $historiesModel->addFieldToFilter('parent_id', $orderId);
        $historiesModel->addFieldToFilter('comment', ['like' => '%' . $description . '%']);
        $historiesModel->load();
        return count($historiesModel->getItems());
    }

    /**
     * @param array $product
     * @param $customerInfo
     * @param $paymentInfo
     * @param OrderInterface $order
     * @param int $OrderException
     * @param int $interation
     * @return array
     */
    public function createOrderSAP($product = [], $customerInfo, $paymentInfo, OrderInterface $order, $OrderException = 0, $interation = 0)
    {
        $idOrderSAP = $order->getData('sap_id');
        $idMagento = $order->getIncrementId();
        if (is_numeric($idOrderSAP) && $idOrderSAP > 0) {
            return [
                'status' => 200,
                'body' => '"err": "ErrorCode -> -1116, ErrorDesc -> (1) ' . $idOrderSAP . '"'
            ];
        }
        try {
            $serie = $this->data->getSerie();
            if (empty($serie) || $serie == null) {
                return [
                    'status' => 999,
                    'body' => '"err": "ErrorCode -> -1116, ErrorDesc -> (1) La serie  no esta definida"'
                ];
            }
            $regionName = $order->getBillingAddress()->getRegion();
            $region = $this->regionFactory->create();
            $region->loadByName($regionName, $order->getBillingAddress()->getCountryId());
            if ($region->getCode() != null) {
                $regionCodeBilling = $region->getCode();
            } else {
                return [
                    'status' => 500,
                    'body' => '"err": "ErrorCode -> -1116, ErrorDesc -> (1) El departamento de facturación ' . $regionName . ' no esta definido en la orden '
                ];
            }
            $regionName = $order->getShippingAddress()->getRegion();
            $region = $this->regionFactory->create();
            $region->loadByName($regionName, $order->getShippingAddress()->getCountryId());
            if ($region->getCode() != null) {
                $regionCodeShipping = $region->getCode();
            } else {
                return [
                    'status' => 500,
                    'body' => '"err": "ErrorCode -> -1116, ErrorDesc -> (1) El departamento de envío ' . $regionName . ' no esta definido en la orden ' . $idMagento . '"'
                ];
            }
            $incrementId = $this->validateNumberOrderMagento($idMagento);
            if ($idMagento != $incrementId) {
                $idMagento = $incrementId;
                $order->setIncrementId($idMagento);
                $this->orderRepository->save($order);
            }
            $attributes = [];
            if ($order->getCustomerId() != null) {
                $customer = $this->customerRepository->getById($order->getCustomerId());
                foreach (['sap_customer_id'] as $attribute) {
                    $attributeObject = $customer->getCustomAttribute($attribute);
                    $attributes[$attribute] = $attributeObject ? $attributeObject->getValue() : null;
                }
            }
            $data = $order->getData();
            $fullName = $data['customer_firstname'] . ' ' . $data['customer_middlename'] . ' ' . $data['customer_lastname'];
            if (empty(trim($fullName))) {
                $fullName = trim($order->getShippingAddress()->getFirstName() . ' ' . $order->getShippingAddress()->getLastName());
            }

            $paymenTitle = $order->getPayment()->getMethodInstance()->getTitle();
            $PaymentCode = $order->getPayment()->getMethodInstance()->getCode();

            $slpcode = $this->data->getSlpCode();
            $comments = $this->storeManager->getStore()->getName() . ' #%s pago:%s email:%s';
            $comments = sprintf($comments, $idMagento, $paymenTitle, $order->getCustomerEmail());
            $observation = '';

            $this->printConsole([
                'incrementId' => $idMagento,
                'paymentMethod' => $PaymentCode,
                'status' => $order->getStatus()
            ]);

            /**
             * Gestión para el comentario longitud maxima en SAP 254
             */
            $c = $comments . "\n" . $observation;
            if (strlen($c) > 250) {
                $comments = $observation;
            } else {
                $comments = $c;
            }
            $u_ser_est = $this->data->getUserEst();
            $u_ser_pe = $this->data->getUserPe();
            $u_num_autor = $this->data->getNumAutor();
            $u_tipo_comprob = $this->data->getTipoComprob();
            $u_fecha_emi_doc_rel = $this->dateTime->date('Y/m/d');
            $u_doc_declarable = $this->data->getDocDeclarable();
            $currentDate = $this->dateTime->date('Y-m-d');
            if ($currentDate <= '2021-11-08') {
                $userFields = null;
                $customerInfo['CamposUsuario'] = null;
            } else {
                $userFields = [
                    "U_SER_EST~$u_ser_est",
                    "U_SER_PE~$u_ser_pe",
                    "U_NUM_AUTOR~$u_num_autor",
                    "U_tipo_comprob~$u_tipo_comprob",
                    "U_fecha_emi_doc_rel~$u_fecha_emi_doc_rel",
                    "U_DOC_DECLARABLE~$u_doc_declarable"
                ];
                $userFields = trim(implode("|", $userFields));
            }
            $shippingAmount = $order->getShippingAmount();
            $discount = $this->discountPercent($order);
            $expenses = [
                "ExpnsCode" => 1,
                "LineTotal" => $this->formatPrice($shippingAmount),
                "TaxCode" => 'IVA',
                "DistributionRule" => $this->data->getWhsCode()
            ];
            $sAddress = $order->getShippingAddress()->getStreet();
            $bAddress = $order->getBillingAddress()->getStreet();
            $shippingAddress = '';
            $billingAddress = '';
            foreach ($sAddress as $address) {
                if (isset($address)) {
                    $shippingAddress = $shippingAddress . $address . ' ';
                }
            }
            foreach ($bAddress as $address) {
                if (isset($address)) {
                    $billingAddress = $billingAddress . $address . ' ';
                }
            }
            $this->dataToSAP = [
                'Opcion' => 0,
                'ObjType' => 13,
                "DocDueDate" => $this->dateTime->date('Y-m-d'),
                "Serie" => $serie,
                'CamposUsuario' => $userFields,
                'SlpCode' => $slpcode,
                "OwnerCode" => "",
                'CiudadS'  => $order->getShippingAddress()->getCity(),
                'RegionS'  => $regionCodeShipping,
                'DireccionS' => $shippingAddress,
                'CountryS' => 'EC',
                'CiudadB'  => $order->getBillingAddress()->getCity(),
                'StateB'  => $regionCodeBilling,
                'DireccionB' => $billingAddress,
                'CountryB' => 'EC',
                'DiscountPercent' => $discount,
                'Detalles' => $product,
                'BusinessPartner' => $customerInfo,
                'Pago' => $paymentInfo,
                'Gastos' => $expenses
                //'ShipToCode' => $order->getBillingAddress()->getCity() . '/' . $regionCodeBilling . '/' . $order->getShippingAddress()->getStreet()[0],
            ];
            $this->write($this->dataToSAP);
            $this->emptyPlacetoPayFormat();
            $response = $this->data->postRecourse('api/Documento/Create', $this->dataToSAP);
            return $response;
        } catch (\Exception $e) {
            return ['status' => 5001, 'body' => $e->getMessage()];
        }
    }

    /**
     * Validate the incrementId
     * @param $incrementID
     * @return bool|mixed
     * @author  Carlos Hernan Aguilar <caguilar@aventi.co>
     * @date 9/11/18
     */
    public function validateNumberOrderMagento($incrementID)
    {
        if (strlen($incrementID) > 10) {
            $incrementID = 'U' . substr($incrementID, 1, 9);
        }
        return $incrementID;
    }

    /**
     * @param $str string of sap
     * @return bool|mixed
     * @author  Carlos Hernan Aguilar <caguilar@aventi.co>
     * @date 9/11/18
     */
    public function validateNumberOrder($str)
    {
        $re = '/("DocNum": ?\d{1,10})/m';
        $numberOrder = false;
        preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);
        if (is_array($matches) && !empty($matches)) {
            $numberOrder = str_replace(['"DocNum":', ' '], '', $matches[0][0]);
        }

        return ($numberOrder == false) ? $str : $numberOrder;
    }

    /**
     * Get error description
     *
     * @param $str
     * @return mixed
     * @author  Carlos Hernan Aguilar <caguilar@aventi.co>
     * @date 22/02/19
     */
    public function getErrorDescription($str)
    {
        try {
            $description = str_replace(['"err":"', '"', "{", "}"], '', $str);
            return $description;
        } catch (\Exception $e) {
            return $str;
        }
    }
    /**
     * Get time process
     *
     * @return float
     */
    public function microtime_float()
    {
        list($useg, $seg) = explode(" ", microtime());
        return ((float)$useg + (float)$seg);
    }

    /**
     * Return all time execute
     *
     * @param $start
     * @return string
     */
    public function getTotalMicroTime($start)
    {
        $end = $this->microtime_float();
        $totalTime = ($end - $start) / 60;
        return number_format($totalTime, 2, ',', ' ') . ' Min ';
    }

    /**
     * Generate the structure of product for SAP
     *
     * @param OrderInterface $orderEntity
     * @return array
     * @author  Carlos Hernan Aguilar <caguilar@aventi.co>
     * @date 26/11/18
     */
    public function getStringProductForSAP(OrderInterface $orderEntity)
    {
        $products = [];
        $items = $orderEntity->getAllVisibleItems();
        foreach ($items as $item) {
            $originalPrice = $item->getOriginalPrice();
            if ($item->getPrice() > 0) {
                $tax = $this->getTax($item->getTaxPercent());
                $products[] = [
                    'ItemCode' => $item->getSku(),
                    'Quantity' => intval($item->getQtyOrdered()),
                    'WhsCode' =>  $this->data->getWhsCode(),
                    'Price' => $this->formatPrice($originalPrice),
                    "TaxCode" => $tax,
                    "DiscountPercent" => 0,
                    "OcrCode" => $this->data->getOcrCode(),
                    "CamposUsuario" =>  null
                ];
            }
        }
        return $products;
    }

    /**
     * @param array $status
     * @return array[]
     */
    public function processOrder($status = [])
    {
        $this->write($status);
        $start = $this->microtime_float();
        $orders = $this->orderCollectionFactory->create()
            ->addAttributeToFilter('status', $status)
            ->addAttributeToFilter('sap_id', ['null' => true])
            ->addAttributeToFilter('created_at', ['gt' => '2021-03-19'])
            ->setOrder('created_at', 'DESC');
        $totalOrders = count($orders);
        $totalOrderSentSAP = $totalOrderError = 0;
        $this->write("Numero de ordenes  $totalOrders");
        foreach ($orders as $orderData) {
            try {
                $order = $this->orderRepository->get($orderData['entity_id']);
                $iteration = $this->getNumberInteration(['syncing', 'error_creacion'], $order->getId());
                if ($iteration == 10) {
                    $order->addStatusToHistory('syncing', sprintf('Sincronizando pedido con SAP Server (%s intento)', $iteration));
                    $this->orderRepository->save($order);
                    $order->addStatusToHistory('syncing', 'Número de intentos máximos superados');
                    $this->orderRepository->save($order);
                    continue;
                } elseif ($iteration > 10) {
                    continue;
                }
                $customerInfo = $this->getStringCustomerInfoForSAP($order);
                $products = $this->getStringProductForSAP($order);
                $paymentInfo = $this->getStringPaymentInfo($order);
                $order->addStatusToHistory('syncing', sprintf('Sincronizando pedido con SAP Server (%s intento)', $iteration));
                $this->orderRepository->save($order);
                $response = $this->createOrderSAP($products, $customerInfo, $paymentInfo, $order, 0, $iteration);
                $this->write($response);
                switch ($response['status']) {
                    case 201:
                        $numberOrder = $this->validateNumberOrder($response['body']);
                        $order->setState('processing');
                        $order->addStatusToHistory(
                            'processing',
                            sprintf('El pedido <strong>%s</strong> fué ingresado en SAP <strong>%s</strong>', $order->getIncrementId(), $numberOrder)
                        )->save();
                        $order->setData('sap_id', $numberOrder);
                        $this->updateSaleOrderGrid($order->getIncrementId(), $numberOrder);
                        $totalOrderSentSAP++;
                        break;
                    case 100:
                        $numberOrder = $this->validateNumberOrder($response['body']);
                        if (is_numeric($numberOrder)) {
                            $order->setData('sap_id', $numberOrder);
                            $this->updateSaleOrderGrid($order->getIncrementId(), $numberOrder);
                            $order->setState('processing');
                            $order->addStatusToHistory(
                                'processing',
                                sprintf('El pedido <strong>%s</strong> fué ingresado en SAP <strong>%s</strong>', $order->getIncrementId(), $numberOrder)
                            )->save();
                            $totalOrderSentSAP++;
                        } else {
                            $errorDescription = sprintf('<strong>Error de creación</strong><br>%s', $this->getErrorDescription($response['body']));
                            if ($this->validateError($errorDescription, $order->getId()) <= 0) {
                                $order->addStatusToHistory('error_creacion', $errorDescription)->save();
                            }
                            $totalOrderError++;
                            $this->logger->error(json_encode($this->dataToSAP));
                        }
                        break;
                    default:
                        $errorDescription = sprintf('<strong>Error de creación</strong><br>%s', $this->getErrorDescription($response['body']));
                        if ($this->validateError($errorDescription, $order->getId()) <= 0) {
                            $order->addStatusToHistory('error_creacion', $errorDescription)->save();
                        }
                        $totalOrderError++;
                        $this->logger->error(json_encode($this->dataToSAP));
                        break;
                }
                $this->orderRepository->save($order);
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }
        $totalTime = $this->getTotalMicroTime($start);
        $reports = [
            'title' => [_('Total orders'), _('Total error'), _('Total completed'), _('Time process')],
            'body' => [
                number_format($totalOrders, 0, ',', '.'),
                number_format($totalOrderError, 0, ',', '.'),
                number_format($totalOrderSentSAP, 0, ',', '.'),
                $totalTime,
            ]
        ];
        return $reports;
    }

    /**
     * @throws \Bcn\Component\Json\Exception\ReadingError
     */
    public function orderCancel()
    {
        $start = 0;
        $row = 1000;
        $webService = Product::WEBSERVICE;
        $date = date('Y-m-d', strtotime($this->dateTime->date('Y-m-d') . ' - 1 days'));
        $method = sprintf('api/Documento/Cancelados/%s/%s/%s/%s', $date, $start, $row, $webService);
        $response = $this->data->getRecourseSelf($method);
        $output = $this->getOutput();
        if ($response != false && $response != null) {
            $reader = $this->getJsonReader($response);
            $reader->enter(\Bcn\Component\Json\Reader::TYPE_OBJECT);
            $total = $reader->read("total");
            $data = $reader->read("data", \Bcn\Component\Json\Reader::TYPE_OBJECT);
            $this->logger->error(json_encode($response));
            if ($total > 0) {
                if ($output) {
                    $progressBar = new ProgressBar($output, $total);
                    $progressBar->start();
                }
                foreach ($data as $order) {
                    $sapID = $order['DocNum'];
                    $orderCollection =  $this->orderCollectionFactory->create()
                        ->addFieldToFilter('sap_id', $sapID);
                    foreach ($orderCollection as $orden) {
                        $order= $this->orderRepository->get($orden->getId());

                        $stateComment = 'Orden Cancelada: <b>' . $sapID . '</b>';
                        $this->orderManagement->cancel($orden->getId());
                        $order->setData('sap_notification_send', 1);
                        $order->addStatusToHistory($order->getStatus(), $stateComment);
                        $this->orderRepository->save($order);
                    }
                    if ($output) {
                        $progressBar->advance();
                    }
                }
                if ($output) {
                    $progressBar->finish();
                }
                $progressBar = null;
            }
        }
    }

    /**
     * Set the sap order id in the sale order grid
     *
     * @param $incrementId
     * @param $orderSapId
     */
    public function updateSaleOrderGrid($incrementId, $orderSapId)
    {
        $table = $this->resourceConnection->getConnection()->getTableName('sales_order_grid');
        $sql = 'UPDATE  __TABLE__ SET sap_id = "__ORDER__" WHERE increment_id  = "__ID__"';
        $search = ['__TABLE__', '__ID__', '__ORDER__'];
        $replace = [$table, $incrementId, $orderSapId];
        $sql = str_replace($search, $replace, $sql);
        $this->resourceConnection->getConnection()->query($sql);
    }

    /**
     * @param $message
     */
    public function printConsole($message)
    {
        /** var \Symfony\Component\Console\Output\OutputInterface  $output */
        $output = $this->getOutput();
        if ($output) {
            if (is_array($message)) {
                $output->writeln(print_r($message, true));
            } else {
                $output->writeln($message);
            }
        }
    }

    /**
     * @param $percent
     * @return string
     */
    public function getTax($percent)
    {
        if ($percent == 12) {
            $taxClass = 'IVA';
        } else {
            $taxClass = 'IVA_0';
        }
        return $taxClass;
    }

    /**
     * @param OrderInterface $orderEntity
     * @return array
     */
    public function getStringCustomerInfoForSAP(OrderInterface $orderEntity)
    {
        $customer = [];
        $bAddress = $orderEntity->getBillingAddress()->getStreet();
        $billingAddress = '';
        foreach ($bAddress as $address) {
            if (isset($address)) {
                $billingAddress = $billingAddress . $address . ' ';
            }
        }
        $identification = $orderEntity->getBillingAddress()->getVatId();
        $firstName = $orderEntity->getBillingAddress()->getFirstName();
        $lastName = $orderEntity->getBillingAddress()->getLastName();
        $fullName = $firstName . ' ' . $lastName;
        $groupCode = $this->data->getGroupCode();
        $slpCode = $this->data->getSlpCode();
        $telephone = $orderEntity->getBillingAddress()->getTelephone();
        $email = $orderEntity->getBillingAddress()->getEmail();
        $city  = $orderEntity->getBillingAddress()->getCity();
        $regionName = $orderEntity->getBillingAddress()->getRegion();
        $region = $this->regionFactory->create();
        $region->loadByName($regionName, $orderEntity->getBillingAddress()->getCountryId());
        $regionCode = 0;
        if ($region->getCode() != null) {
            $regionCode = $region->getCode();
        }
        $queryGroup2 = $this->data->getQryGroup2();
        $queryGroup3 = $this->data->getQryGroup3();

        $identificationType = $this->sap->returnIdentificationType($identification);
        $identificationType = trim(implode("|", $identificationType));
        $customer = [
            "LicTradNum" => $identification,
            "CardName" => $fullName,
            "GroupCode" => $groupCode,
            "SlpCode" => $slpCode,
            "Telefono" => $telephone,
            "Email" => $email,
            "Direccion" => $billingAddress,
            "Ciudad" => $city,
            "Pais" => "EC",
            "Departamento" => $regionCode,
            "QryGroup2" => $queryGroup2,
            "QryGroup3" => $queryGroup3,
            "CamposUsuario" => $identificationType
        ];
        return $customer;
    }

    /**
     * @param OrderInterface $orderEntity
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getStringPaymentInfo(OrderInterface $orderEntity)
    {
        $paymentCode = $orderEntity->getPayment()->getMethodInstance()->getCode();
        $transferAccount = "";
        $payType = 0;
        switch ($paymentCode) {
            case self::BANKTRANSFERPAYMENT:
                $transferAccount = $this->data->getBankTransfer();
                $payType = $this->data->getTransferType();
                break;
            case self::CASHONDELIVERYPAYMENT:
                $transferAccount = $this->data->getCashOnDelivery();
                $payType = $this->data->getOnDeliveryType();
                break;
            case self::PLACETOPAYPAYMENT:
                $transferAccount = $this->data->getCashOnDelivery();
                $payType = $this->data->getCreditType();
                break;
            default:
                $transferAccount = $this->data->getBankTransfer();
                break;
        }
        $paymentForm = ($paymentCode == self::PLACETOPAYPAYMENT) ? "19" : "20";

        $today = $this->dateTime->date('Y-m-d');
        $this->getPlacetopayAventi($orderEntity->getIncrementId());
        return [
            "TipoPago" => $payType,
            "Series" => $this->data->getPaymentSerie(),
            "CreditCard" => $this->_placetopayArrayFormat[self::CREDIT_CARD],
            "CreditCardNumber" => $this->_placetopayArrayFormat[self::CREDIT_CARD_NUMBER],
            "CardValidUntil" => $this->_placetopayArrayFormat[self::CARD_VALID_UNTIIL],
            "CreditSum" => $this->formatPrice($orderEntity->getGrandTotal()),
            "OwnerIdNum" => $this->_placetopayArrayFormat[self::OWNER_ID_NUM],
            "VoucherNum" => $this->data->getUserEst() . $this->data->getUserPe(),
            "UnidadTiempo" => "Dia",
            "FormaPago" => $paymentForm,
            "DocDate" => $today,
            "TaxDate" => $today,
            "DocDueDate" => $today,
            "TrsfrSum" => $this->formatPrice($orderEntity->getGrandTotal()),
            "TrsfrAcct" => $transferAccount,
            "TrsfrRef" => $orderEntity->getIncrementId(),
            "Comments" => "",
            "CamposUsuario" => $this->_placetopayArrayFormat[self::USER_FIELDS]
        ];
    }

    /**
     * Instance the class reader for json
     *
     * @param $filePath
     * @return Reader
     * @author  Carlos Hernan Aguilar <caguilar@aventi.co>
     * @date 14/11/18
     */
    public function getJsonReader($filePath)
    {
        if (file_exists($filePath)) {
            $this->file = fopen($filePath, "r");
            return new Reader($this->file);
        }
    }

    /**
     * @param $price
     * @return string
     */
    public function formatPrice($price)
    {
        return number_format($price, 2, '.', '');
    }

    public function getPlacetopayAventi($incrementId)
    {
        $placetoPayAventi = $this->_placetopayAventiCollectionFactory->create()
            ->addFieldToFilter('increment_id', ['eq' => $incrementId]);
        foreach ($placetoPayAventi->getData() as $data) {
            if ($data['status'] == 'APPROVED') {
                $this->formatResponse(json_decode($data['value'], true));
            }
        }
    }

    private function formatResponse($response)
    {
        if (is_array($response)) {
            if (isset($response['payment'])) {
                foreach ($response['payment'] as $res) {
                    if (isset($res['processorFields'])) {
                        $this->processOtherFields($res['processorFields'], $res['receipt']);
                        $paymentMethod = $this->validatePaymentMethodToSap($res['paymentMethod']);
                        $this->_placetopayArrayFormat[self::OWNER_ID_NUM] = $res['authorization'];
                    }
                }
            }
        }
    }

    private function validatePaymentMethodToSap($paymentMethod)
    {
        $payment = '';
        $creditCard = 0;
        switch ($paymentMethod) {
            case 'discover':
                $payment = 'discover';
                $creditCard = 34;
                break;
            case 'diners':
                $payment = 'Diners club';
                $creditCard = 33;
                break;
            case 'visa':
                $payment = 'diners visa';
                $creditCard = 35;
                break;
            case 'mastercard':
                $payment = 'diners mastercard';
                $creditCard = 36;
                break;
            case 'PACIFICARD':
                $payment = 'PACIFICARD';
                $creditCard = 37;
                break;
            case 'GUAYAQUIL':
                $payment = 'GUAYAQUIL';
                $creditCard = 38;
                break;
            case 'ALIA':
                $payment = 'ALIA';
                $creditCard = 39;
                break;
            case 'AUSTRO':
                $payment = 'AUSTRO';
                $creditCard = 40;
                break;
            case 'MEDIANET':
                $payment = 'MEDIANET';
                $creditCard = 41;
                break;
            default:
                $payment = $paymentMethod;
                break;
        }
        $this->_placetopayArrayFormat[self::CREDIT_CARD] = $creditCard;
        return $payment;
    }

    private function processOtherFields($otherFields, $numVoucher)
    {
        $batch = 0;
        $lastDigits = 0;
        $expiration = "";
        foreach ($otherFields as $data) {
            switch ($data['keyword']) {
                case 'batch':
                    $batch = $data['value'];
                    break;
                case 'expiration':
                    $expiration = $this->formatCardValid($data['value']);
                    break;
                case 'lastDigits':
                    $lastDigits = $data['value'];
                    break;
                default:
                    break;
            }
        }
        $userFields = [
            "U_CXS_NUM_VOUCHER~$numVoucher",
            "U_CXS_NUM_LOTE~$batch"
        ];
        $userFields = trim(implode("|", $userFields));
        $this->_placetopayArrayFormat[self::CARD_VALID_UNTIIL] = $expiration;
        $this->_placetopayArrayFormat[self::USER_FIELDS] = $userFields;
        $this->_placetopayArrayFormat[self::CREDIT_CARD_NUMBER] = $lastDigits;
    }

    /**
     * @param $cardValid
     * @return array|string|string[]
     */
    private function formatCardValid($cardValid)
    {
        return substr_replace($cardValid, '/', 2, 0);
    }

    private function emptyPlacetoPayFormat()
    {
        $this->_placetopayArrayFormat = [
            self::CREDIT_CARD => null, // Depende de la franquicia
            self::CREDIT_CARD_NUMBER => null, // lastDigits
            self::CARD_VALID_UNTIIL => null, // expiration
            self::OWNER_ID_NUM => null, //authorization
            self::USER_FIELDS => null //U_CXS_NUM_LOTE-> Batch U_CXS_NUM_VOUCHER -> Receipt
        ];
    }

    /**
     * This function calculate the discount percent and send it to SAP
     * @param OrderInterface $order
     * @return float|int
     */
    private function discountPercent(OrderInterface $order)
    {
        $discount = 0;
        $realPrice = $this->realPrice($order);
        if ($order->getBaseDiscountAmount() < 0) {
            $magentoDiscount = abs($order->getBaseDiscountAmount());
            $discount = 100 - ((($realPrice - $magentoDiscount) / $realPrice) * 100);
        } else {
            $priceWithDiscount = $order->getBaseSubtotal();
            if ($realPrice !== $priceWithDiscount && $realPrice !== 0) {
                $discount = 100 - (($priceWithDiscount/$realPrice)*100);
            }
        }
        return abs(round($discount, 2, PHP_ROUND_HALF_DOWN));
    }

    /**
     * This function calculate subtotal based on products in cart
     * @param OrderInterface $order
     * @return float|int
     */
    private function realPrice(OrderInterface $order)
    {
        $price = 0;
        $items = $order->getAllVisibleItems();
        foreach ($items as $item) {
            $itemAmount = $item->getQtyOrdered();
            $itemOriginalPrice = $item->getOriginalPrice();
            $price += ($itemOriginalPrice * $itemAmount);
        }
        return $price;
    }
}
