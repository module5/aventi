<?php


namespace Aventi\SAP\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;


class InstallData implements InstallDataInterface
{

    private $customerSetupFactory;

    /**
     * Constructor
     *
     * @param \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'identification_customer', [
            'type' => 'varchar',
            'label' => 'Identification customer',
            'input' => 'text',
            'source' => '',
            'required' => true,
            'visible' => true,
            'position' => 333,
            'system' => false,
            'backend' => ''
        ]);

        $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'identification_customer')
        ->addData(['used_in_forms' => [
                'adminhtml_customer',
                'adminhtml_checkout',
                'customer_account_create',
                'customer_account_edit'
            ]
        ]);

        $attribute->save();

        $installer = $setup;
        $installer->startSetup();

        $eavQuoteAddress = $installer->getTable('quote_address');
        $eavOrderAddress = $installer->getTable('sales_order_address');

        $columns = [
            'order_type' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'label' => 'order type',
                'input' => 'text',
                'length' => 10,
                'source' => '',
                'required' => false,
                'visible' => true,
                'position' => 444,
                'system' => false,
                'backend' => '',
                'comment' => 'Order Type'
            ]
        ];

        $connection = $installer->getConnection();
        foreach ($columns as $name => $definition) {
            $connection->addColumn($eavQuoteAddress, $name, $definition);
            $connection->addColumn($eavOrderAddress, $name, $definition);
        }

        $installer->endSetup();
    }
}
