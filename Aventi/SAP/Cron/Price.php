<?php

namespace Aventi\SAP\Cron;

use Bcn\Component\Json\Exception\ReadingError;
use Magento\Framework\Exception\FileSystemException;

class Price
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Aventi\SAP\Model\Sync\Price
     */
    private $_price;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Aventi\SAP\Model\Sync\Price $price
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Aventi\SAP\Model\Sync\Price $price
    ) {
        $this->logger = $logger;
        $this->_price = $price;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        $this->logger->info("Price cron job was executed.");
        try {
            $date = "1900-01-01";
            $this->_price->syncPrice($date);
        } catch (ReadingError | FileSystemException $e) {
            $this->logger->debug('There was an error while syncing prices: ' . $e->getMessage());
        }
    }
}
