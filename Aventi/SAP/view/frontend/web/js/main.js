require([
    'jquery',
    'mage/url'
], function ($, url) {

    $(document).on('change', '[name="identification_customer"]', function () {

        var payload = {
                param: $(this).val()

        };

        $.ajax({
            url: BASE_URL + 'rest/V1/aventi-sap/identification',
            global: false,
            contentType: 'application/json',
            type: 'PUT',
            async: false,
            data: JSON.stringify(payload),
            cache: false
         }).done(function (json) {

        });

    });

    $(document).on('click', '[name="invoiceType"]', function(){
        
        var val = $(this).val();
        
        var previousValue = $(this).data('storedvalue');

        $(this).data('storedvalue', !previousValue);

        if(!previousValue){                        
            $(this).prop('checked', false);            
            
            val = $("#radio-invoice").val();  
            $("#radio-invoice").prop('checked', true);            
        }
        
        var payload = {
            param: val

        };        

        $.ajax({
            url: BASE_URL + 'rest/V1/aventi-sap/ordertype',
            global: false,
            contentType: 'application/json',
            type: 'PUT',
            async: false,
            data: JSON.stringify(payload),
            cache: false
        }).done(function (json) {
            
        });
        /*var value = $(this).val();
        if (quote.shippingAddress().extensionAttributes == undefined) {
            quote.shippingAddress().extensionAttributes = {};
        }
        console.log(value);
        console.log(quote.shippingAddress().customAttributes);
        quote.shippingAddress().extensionAttributes.order_type=value;*/
    });   

});


