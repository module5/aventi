<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aventi\SAP\Model;

use Aventi\SAP\Api\Data\PlacetopayAventiInterface;
use Aventi\SAP\Api\Data\PlacetopayAventiInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class PlacetopayAventi extends \Magento\Framework\Model\AbstractModel
{
    protected $placetopayaventiDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'aventi_sap_placetopayaventi';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PlacetopayAventiInterfaceFactory $placetopayaventiDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Aventi\SAP\Model\ResourceModel\PlacetopayAventi $resource
     * @param \Aventi\SAP\Model\ResourceModel\PlacetopayAventi\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        PlacetopayAventiInterfaceFactory $placetopayaventiDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Aventi\SAP\Model\ResourceModel\PlacetopayAventi $resource,
        \Aventi\SAP\Model\ResourceModel\PlacetopayAventi\Collection $resourceCollection,
        array $data = []
    ) {
        $this->placetopayaventiDataFactory = $placetopayaventiDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve placetopayaventi model with placetopayaventi data
     * @return PlacetopayAventiInterface
     */
    public function getDataModel()
    {
        $placetopayaventiData = $this->getData();

        $placetopayaventiDataObject = $this->placetopayaventiDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $placetopayaventiDataObject,
            $placetopayaventiData,
            PlacetopayAventiInterface::class
        );

        return $placetopayaventiDataObject;
    }
}
