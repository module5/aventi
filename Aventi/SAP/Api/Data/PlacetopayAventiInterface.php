<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aventi\SAP\Api\Data;

interface PlacetopayAventiInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const PLACETOPAYAVENTI_ID = 'placetopayaventi_id';
    const STATUS = 'status';
    const VALUE = 'value';
    const INCREMENT_ID = 'increment_id';

    /**
     * Get placetopayaventi_id
     * @return string|null
     */
    public function getPlacetopayaventiId();

    /**
     * Set placetopayaventi_id
     * @param string $placetopayaventiId
     * @return \Aventi\SAP\Api\Data\PlacetopayAventiInterface
     */
    public function setPlacetopayaventiId($placetopayaventiId);

    /**
     * Get increment_id
     * @return string|null
     */
    public function getIncrementId();

    /**
     * Set increment_id
     * @param string $incrementId
     * @return \Aventi\SAP\Api\Data\PlacetopayAventiInterface
     */
    public function setIncrementId($incrementId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Aventi\SAP\Api\Data\PlacetopayAventiExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Aventi\SAP\Api\Data\PlacetopayAventiExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Aventi\SAP\Api\Data\PlacetopayAventiExtensionInterface $extensionAttributes
    );

    /**
     * Get value
     * @return string|null
     */
    public function getValue();

    /**
     * Set value
     * @param string $value
     * @return \Aventi\SAP\Api\Data\PlacetopayAventiInterface
     */
    public function setValue($value);

    /**
     * Get status
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     * @param string $status
     * @return \Aventi\SAP\Api\Data\PlacetopayAventiInterface
     */
    public function setStatus($status);
}

