<?php

namespace Aventi\SAP\Model\Sync;

use Aventi\SAP\Helper\Attribute;
use Aventi\SAP\Helper\Data;
use Aventi\SAP\Model\AbstractSync;
use Bcn\Component\Json\Exception\ReadingError;
use Bcn\Component\Json\Reader;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem\DriverInterface;
use Magento\InventoryApi\Api\Data\SourceItemInterfaceFactory;
use Magento\InventoryApi\Api\SourceItemsSaveInterface;
use Psr\Log\LoggerInterface;

class Stock extends AbstractSync
{
    const DEFAULT_SOURCE = 'default';
    const WEBSERVICE = 2;

    /**
     * @var Data
     */
    private $data;

    /**
     * @var StockRegistryInterface
     */
    private $stockRegistry;

    /**
     * @var Check
     */
    private $check;

    /**
     * @var SourceItemInterfaceFactory
     */
    private $sourceItemFactory;

    /**
     * @var SourceItemsSaveInterface
     */
    private $sourceItemsSaveInterface;

    /**
     * @param Attribute $attributeDate
     * @param LoggerInterface $logger
     * @param DriverInterface $driver
     * @param ProductRepositoryInterface $productRepository
     * @param SourceItemsSaveInterface $sourceItemsSaveInterface
     * @param SourceItemInterfaceFactory $sourceItemFactory
     * @param StockRegistryInterface $stockRegistry
     * @param Data $data
     * @param Check $check
     */
    public function __construct(
        Attribute $attributeDate,
        LoggerInterface $logger,
        DriverInterface $driver,
        SourceItemsSaveInterface $sourceItemsSaveInterface,
        SourceItemInterfaceFactory $sourceItemFactory,
        StockRegistryInterface $stockRegistry,
        Data $data,
        Check $check,
        \Magento\Catalog\Model\ResourceModel\Product $productResource
    ) {
        parent::__construct($attributeDate, $logger, $driver, $productResource);
        $this->sourceItemsSaveInterface = $sourceItemsSaveInterface;
        $this->sourceItemFactory = $sourceItemFactory;
        $this->stockRegistry = $stockRegistry;
        $this->data = $data;
        $this->check = $check;
    }

    /**
     * Update the stock product
     *
     * @param int $option
     * @throws ReadingError
     * @throws FileSystemException
     */
    public function syncStock(int $option = 0)
    {
        $resTable = ['check' => 0, 'updated' => 0, 'new' => 0];
        $start = 0;
        $rows = 1000;
        $flag = true;
        $webService = Stock::WEBSERVICE;
        $method = ($option == 0) ? 'api/Producto/Stock/%s/%s/%s' : 'api/Producto/StockRapido/%s/%s/%s';
        while ($flag) {
            $jsonPath =  $this->data->getRecourseSelf(sprintf($method, $start, $rows, $webService));
            if ($jsonPath) {
                $reader = $this->getJsonReader($jsonPath);
                $reader->enter(null, Reader::TYPE_OBJECT);
                $total = $reader->read("total");
                $products = $reader->read("data");
                $progressBar = $this->startProgressBar($total);
                foreach ($products as $product) {
                    $stock = (object) [
                        'sku' => $product['ItemCode'],
                        'qty' => ($product['STOCK'] <= 0) ? 0 : $product['STOCK'],
                        'isInStock' => ($product['STOCK'] <= 0) ? 0 : 1
                    ];
                    $result = $this->stockManager($stock);
                    $resTable['check'] += $result['check'];
                    $resTable['updated'] += $result['updated'];
                    $this->advanceProgressBar($progressBar);
                    //Debug only
                    //$total--;
                }
                $start += $rows;
                $this->finishProgressBar($progressBar, $start, $rows);
                $progressBar = null;
                $this->closeFile($jsonPath);
                if ($total <= 0) {
                    $flag = false;
                }
            } else {
                $flag = false;
            }
        }
        $this->printTable($resTable);
    }

    /**
     * @param $data
     * @return int[]
     */
    public function stockManager($data): array
    {
        $result = ['updated' => 0, 'new' => 0, 'check' => 0];

        try {
            $stockItem = $this->stockRegistry->getStockItemBySku($data->sku);
            $resultCheck = $this->check->checkData($data, $stockItem, 'stock', 1);
            if (!$resultCheck) {
                $result['check'] = 1;
                return $result;
            }
            $result['updated'] = 1;
            $stockStatus = ($data->qty > 0) ? 1 : 0;
            $sourceItem = $this->sourceItemFactory->create();
            $sourceItem->setSourceCode(self::DEFAULT_SOURCE);
            $sourceItem->setSku($data->sku);
            $sourceItem->setQuantity($data->qty);
            $sourceItem->setStatus($stockStatus);
            $this->sourceItemsSaveInterface->execute([$sourceItem]);
        } catch (LocalizedException $e) {
            $this->logger->error($e->getMessage());
        }
        return $result;
    }
}
