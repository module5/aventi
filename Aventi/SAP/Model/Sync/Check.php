<?php

namespace Aventi\SAP\Model\Sync;

use Magento\Catalog\Api\Data\ProductInterface;
use Psr\Log\LoggerInterface;
use UnexpectedValueException;

class Check
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Check constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    /**
     * Check the data for differences.
     *
     * @param object $data The new data to compare
     * @param object $item The original data from DB
     * @param string $option The type of data to be checked. There are 3 types:
     * product, stock and price.
     * @param int $visibility
     * @return bool|array returns <b>FALSE</b> if there aren't
     * differences, otherwise returns the array containing the data.
     */
    public function checkData(object $data, object $item, string $option, int $visibility)
    {
        switch ($option) {
            case 'product':
                $currentData = [
                    'sku' =>  $data->sku,
                    'name' => $data->name,
                    'tax_class_id' => $data->tax,
                    'status' => $data->status,
                    'codeBars' => $data->codeBars,
                    'weight' => (float) $data->weight,
                    'color' => $data->color,
                    'talla' => $data->talla,
                    'estilo' => $data->estilo,
                    'fit' => $data->fit,
                    'visibility' => $visibility,
                    'shortDescription' => $data->shortDescription,
                    'description' => $data->description
                ];
                $headData = [
                    'sku' =>  $item->getData('sku'),
                    'name' => $item->getData('name'),
                    'tax_class_id' => $item->getData('tax_class_id'),
                    'status' => $item->getData('status'),
                    'codeBars' => $item->getData('bar_code'),
                    'weight' => (float) $item->getWeight(),
                    'color' => $item->getData('color'),
                    'talla' => $item->getData('talla'),
                    'estilo' => $item->getData('estilo'),
                    'fit' => $item->getData('fit'),
                    'visibility' => $item->getData('visibility'),
                    'shortDescription' => $item->getShortDescription(),
                    'description' => $item->getDescription()
                ];
                break;
            case 'price':
                $currentData = [
                    'price' => $data->price
                ];
                $headData = [
                    'price' => (int) $item->getPrice()
                ];
                break;
            case 'stock':
                $currentData = [
                    'qty' => $data->qty
                ];
                $headData = [
                    'qty' => $item->getQty()
                ];
                break;
            default:
                throw new UnexpectedValueException('Unexpected value');
        }
        $checkData = array_diff_assoc($currentData, $headData);
        if (empty($checkData)) {
            return false;
        }
        return $checkData;
    }

    /**
     * Checks categories product and retrieves differences.
     * @param object $data
     * @param ProductInterface $product
     * @return array|false returns <b>FALSE</b> if there aren't
     * differences, otherwise returns an array containing the data.
     */
    public function checkCategories(object $data, ProductInterface $product)
    {
        $arrayValues = [];
        if (is_array($data->categoryId)) {
            $arrayValues = array_values($data->categoryId);
        }
        $categoryDiff = array_diff($product->getCategoryIds(), $arrayValues);

        if (empty($categoryDiff)) {
            return false;
        }
        return $categoryDiff;
    }
}
