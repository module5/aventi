<?php


namespace Aventi\SAP\Api;

/**
 * Interface OrderTypeManagmentInterface
 *
 * @package Aventi\SAP\Api
 */
interface OrderTypeManagmentInterface
{
    /**
     * PUT for order type api
     * @param string $param
     * @return string
     */
    public function saveOrderOrderType($param);
}
