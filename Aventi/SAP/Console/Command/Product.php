<?php
namespace Aventi\SAP\Console\Command;

use Exception;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Product extends Command
{
    const PROCESS_SELECTED = 1;

    /**
     * @var \Aventi\SAP\Model\Sync\Product
     */
    private $_product;

    /**
     * @var DateTime
     */
    private $dateTime;

    /**
     * @param \Aventi\SAP\Model\Sync\Product $product
     * @param DateTime $dateTime
     */
    public function __construct(
        \Aventi\SAP\Model\Sync\Product $product,
        DateTime $dateTime
    ) {
        $this->_product = $product;
        $this->dateTime = $dateTime;
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('aventi:sap:product');
        $this->setDescription('Sync product SAP to Magento. Argument: 0 => Product, 1 => Fast Product');
        $this->setDefinition([
           new InputArgument(self::PROCESS_SELECTED, null,  "Process")
        ]);
        parent::configure();
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $customDate = "1900-01-01";
        $process = (int) $input->getArgument(self::PROCESS_SELECTED);
        if ($process < 0 || $process > 1) {
            $output->writeln("<error>Process no found :(</error>");
            throw new InputException(__('Invalid argument.'));
        } elseif ($process == 1) {
            $customDate = date('Y-m-d', strtotime($this->dateTime->date('Y-m-d')));
        }
        $this->_product->setOutput($output);
        $this->_product->syncProduct($customDate);
    }
}
