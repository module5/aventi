<?php


namespace Aventi\SAP\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Quote\Setup\QuoteSetupFactory;

class UpgradeData implements UpgradeDataInterface
{

    private $eavSetupFactory;
    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;
    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;
    /**
     * @var SalesSetupFactory
     */
    private $salesSetupFactory;
    /**
     * @var QuoteSetupFactory
     */
    private $quoteSetupFactory;

    /**
     * Constructor
     *
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        AttributeSetFactory $attributeSetFactory,
        CustomerSetupFactory $customerSetupFactory,
        SalesSetupFactory $salesSetupFactory,
        QuoteSetupFactory $quoteSetupFactory
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->salesSetupFactory = $salesSetupFactory;
        $this->quoteSetupFactory = $quoteSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        /*$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        if (version_compare($context->getVersion(), "1.0.1", "<")) {

            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
            $customerAddressEntity = $customerSetup->getEavConfig()->getEntityType('customer_address');
            $attributeSetId = $customerAddressEntity->getDefaultAttributeSetId();


            $attributeSet = $this->attributeSetFactory->create();
            $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

            $customerSetup->addAttribute('customer_address', 'identification_customer', [
                'type'          => 'varchar',
                'label'         => 'Número de identificación',
                'input'         => 'text',
                'required'      =>  false,
                'visible'       =>  true,
                'user_defined'  =>  true,
                'sort_order'    =>  30,
                'position'      =>  30,
                'system'        =>  0,
            ]);

            $attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'identification_customer')
                ->addData([
                    'attribute_set_id' => $attributeSetId,
                    'attribute_group_id' => $attributeGroupId
                ]);
            $attribute->save();

        }


        if (version_compare($context->getVersion(), "1.0.2", "<")) {

            $salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);
            $salesSetup->addAttribute('order_address', 'identification_customer',
                [
                    'type' => 'varchar',
                    'length' => 30,
                    'visible' => false,
                    'required' => false,
                    'grid' => false
                ]
            );
        }

        if (version_compare($context->getVersion(), "1.0.3", "<")) {

            $quoteSetup = $this->quoteSetupFactory->create(['setup' => $setup]);
            $quoteSetup->addAttribute('quote_address', 'identification_customer',
                [
                    'type' => 'varchar',
                    'length' => 30,
                    'visible' => false,
                    'required' => false,
                    'grid' => false
                ]
            );
        }*/




    }
}
