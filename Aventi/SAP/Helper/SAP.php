<?php


namespace Aventi\SAP\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class SAP extends AbstractHelper
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    )
    {
        parent::__construct($context);
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * get postal code of table temporal
     *
     * @param $region
     * @param $city
     * @method
     * date 20/06/19/09:07 AM
     * @author Carlos Hernan Aguilar Hurtado <caguilar@aventi.co>
     * @return int|string|null
     */
    public function getPostalCode($region, $city)
    {
        $connection = $this->resourceConnection->getConnection();
        $sql = 'SELECT id from aventi_sap_cities where region like "%' . $region . '%"  and name like "%' . $city . '%"';
        $postalCode = $connection->fetchOne($sql);
        return (is_numeric($postalCode)) ? $postalCode : null;
    }

    /**
     *
     * Get the customer id
     *
     * @param $cn
     * @method
     * date 20/06/19/11:03 AM
     * @author Carlos Hernan Aguilar Hurtado <caguilar@aventi.co>
     * @return int|string|null
     */
    public function getCustomerId($cn)
    {
        $connection = $this->resourceConnection->getConnection();
        $sql = 'SELECT entity_id from customer_entity_varchar where `value` = "' . $cn . '"';
        $id = $connection->fetchOne($sql);
        return (is_numeric($id)) ? $id : null;
    }


    public function managerCustomerAddressSAP($address, $addressId = null)
    {

        $connection = $this->resourceConnection->getConnection();
        $sql = 'SELECT entity_id from customer_address_entity where `sap` = "' . addslashes($address) . '"';
        $id = $connection->fetchOne($sql);
        if (is_numeric($id)) {
            return $id;
        } else if (is_numeric($addressId)) {

            $sql = 'UPDATE customer_address_entity  SET `sap` = "' . addslashes($address) . '" WHERE  entity_id = ' . (int)$addressId;
            $connection->query($sql);
            return $addressId;
        }
        return null;
    }


    public function getCategory($sap)
    {
        $connection = $this->resourceConnection->getConnection();
        $sql = 'SELECT entity_id from catalog_category_entity_varchar where `value` = "' . trim($sap) . '" and  attribute_id = 180 ';
        $id = $connection->fetchOne($sql);
        return (is_numeric($id)) ? $id : null;
    }

    public function getCategoryByName($GranFamilia, $familia, $subFamilia)
    {
        $connection = $this->resourceConnection->getConnection();
        $sql = <<<SQL
        SELECT su.entity_id
        FROM catalog_category_entity su
        INNER JOIN  catalog_category_entity fa on (fa.entity_id = su.parent_id)
        INNER JOIN  catalog_category_entity gran on (gran.entity_id = fa.parent_id)
        INNER JOIN catalog_category_entity_varchar sun on (sun.entity_id = su.entity_id and sun.attribute_id = 45 and sun.value = '__SUBFAMILIA__')
        INNER JOIN catalog_category_entity_varchar fan on (fan.entity_id = fa.entity_id and fan.attribute_id = 45 and fan.value = '__FAMILIA__')
        INNER JOIN catalog_category_entity_varchar grann on (grann.entity_id = gran.entity_id and grann.attribute_id = 45 and grann.value = '__GRANFAMILIA__')
SQL;

        $sql = str_replace(['__SUBFAMILIA__', '__FAMILIA__', '__GRANFAMILIA__'], [$subFamilia, $familia, $GranFamilia], $sql);


        $id = $connection->fetchOne($sql);
        return (is_numeric($id)) ? $id : null;
    }

    public function getCategoryParent($sap)
    {
        $connection = $this->resourceConnection->getConnection();
        $sql = '
                SELECT c.path,v.entity_id
                FROM `catalog_category_entity_varchar` v
                INNER JOIN catalog_category_entity c on (c.entity_id = v.entity_id)
                WHERE v.`attribute_id` = 180 and v.value =  "' . $sap . '"
                        ';
        return $connection->fetchRow($sql);
    }


    public function getAddressSAP($addressId)
    {

        $connection = $this->resourceConnection->getConnection();
        $sql = 'SELECT sap from customer_address_entity where `entity_id` = "' . $addressId . '"';
        $id = $connection->fetchOne($sql);
        if ($id != null) {
            return $id;
        }
        return null;
    }

    /**
     *  Get in SKU  in base to itemCode
     * @param $sapId
     * @return string|null
     */
    public function getSkuBySAP($sapId)
    {

        $connection = $this->resourceConnection->getConnection();
        $sql=<<<SQL
             SELECT sku
             FROM `eav_attribute` e
             INNER JOIN catalog_product_entity_varchar v on (e.attribute_id = v.attribute_id )
             INNER JOIN catalog_product_entity p on (p.entity_id = v.entity_id )
             where e.attribute_code = 'sap'
             and e.entity_type_id = '4'
             and v.value = 'ID'
             and v.store_id = 1
SQL;
        $id = $connection->fetchOne( str_replace('ID',$sapId,$sql));
        if ($id != null) {
            return $id;
        }
        return null;
    }



    public function getCatalogBySku($sapId)
    {

        $connection = $this->resourceConnection->getConnection();
        $sql=<<<SQL
             SELECT v.value
             FROM `eav_attribute` e
             INNER JOIN catalog_product_entity_varchar v on (e.attribute_id = v.attribute_id )
             INNER JOIN catalog_product_entity p on (p.entity_id = v.entity_id )
             where e.attribute_code = 'sap'
             and e.entity_type_id = '4'
             and (p.sku = 'ID' or  v.value = 'ID')
             and v.store_id = 1
SQL;
        $id = $connection->fetchOne( str_replace('ID',$sapId,$sql));
        if ($id != null) {
            return $id;
        }
        return null;
    }

    public function getLastCategory($category)
    {
        $connection = $this->resourceConnection->getConnection();
        $id = '';
        if ($category) {
            $sql = <<<SQL
            SELECT su.entity_id
            FROM catalog_category_entity su
            INNER JOIN catalog_category_entity_varchar sun on (sun.entity_id = su.entity_id and sun.attribute_id = 45 and sun.value = '__CATEGORY__')
            SQL;
            $sql = str_replace(['__CATEGORY__'], [$category], $sql);
            $id = $connection->fetchOne($sql);
        }
        return (is_numeric($id)) ? $id : null;
    }

    /**
     * @param $identification
     * @return array|string[]
     */
    public function returnIdentificationType($identification)
    {
        //Cédula
        $arrayToReturn = [
            "U_TIPO_RUC~N",
            "U_TIPO_CONTR~N",
            "U_TIPO_ID~C",
            "U_INTRX_FM_Sexo~0",
            "U_Exx_TProv~01"
        ];
        if (strlen($identification) == 10) {
            $arrayToReturn = [
                "U_TIPO_RUC~N",
                "U_TIPO_CONTR~N",
                "U_TIPO_ID~C",
                "U_INTRX_FM_Sexo~0",
                "U_Exx_TProv~01"
            ];
        } elseif (strlen($identification) == 13) {
            $arrayToReturn = [
                "U_TIPO_RUC~NR",
                "U_TIPO_CONTR~NR",
                "U_TIPO_ID~R",
                "U_INTRX_FM_Sexo~0",
                "U_Exx_TProv~01"
            ];
        }
        return $arrayToReturn;
    }
}
