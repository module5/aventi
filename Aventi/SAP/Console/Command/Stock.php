<?php
namespace Aventi\SAP\Console\Command;

use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Stock extends Command
{
    const PROCESS_SELECTED = 'Process';

    private \Aventi\SAP\Model\Sync\Stock $_stock;

    public function __construct(
        \Aventi\SAP\Model\Sync\Stock $stock
    ) {
        $this->_stock = $stock;
        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('aventi:sap:stock');
        $this->setDescription('Update products stock from SAP');
        $this->setDefinition([
            new InputArgument(self::PROCESS_SELECTED, null,  "Process")
        ]);
        parent::configure();
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $process = (int)$input->getArgument(self::PROCESS_SELECTED);
        if ($process < 0 || $process >1) {
            $output->writeln("<error>Process no found :(</error>");
            exit;
        }
        $this->_stock->setOutput($output);
        $this->_stock->syncStock($process);
    }
}
